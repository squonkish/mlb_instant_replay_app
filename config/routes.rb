MlbApp::Application.routes.draw do

  
  resources :searches

  resources :manuals

  resources :calendars

  root "replays#new"
  get "contact", to: "pages#contact", as: "contact"
 
  devise_for :users
  resources :users
  

  namespace :admin do
    root "base#index"
    resources :users
     end


 
  resources :replays, :emails, :confirmed, :calendars, :manual, :search
  
   

end
