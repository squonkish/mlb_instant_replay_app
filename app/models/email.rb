class Email < ActiveRecord::Base
  belongs_to :replay

  validates_format_of :receiver_address,  with: /@/ , :message => "Please enter valid email address."

  $new_replay = Replay.new
  $new_email = Email.new

end


# from: http://www.mlb.com/lookup/named.schedule_vw.bam?season=2015&game_date=%272015-03-16%27&sport_code=%27mlb%27&sport_code=%27win%27
class GameFeed
  include HTTParty
  base_uri 'gd2.mlb.com/components/game/mlb'

  def self.find(year,month, day)
    get("/year_#{year}/month_#{month}/day_#{day}/grid.json")
  end

end

# from: http://d3ypgbzni7egm.cloudfront.net/api/v1/game/429912/feed/live
class PlayersFeed
  include HTTParty
  base_uri 'gd2.mlb.com/components/game/mlb'

  def self.find(year,month, day, game_id)
    get("/year_#{year}/month_#{month}/day_#{day}/gid_#{game_id}/players.xml")
  end

end