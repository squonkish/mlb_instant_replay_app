class Confirmed < ActiveRecord::Base

	 $new_replay = Replay.new
  $new_email = Email.new


	def self.to_csv
    CSV.generate do |csv|
      csv << column_names
      all.each do |replay|
        csv << replay.attributes.values_at(*column_names)
      end
    end
  end


end

  # from: http://www.mlb.com/lookup/named.schedule_vw.bam?season=2015&game_date=%272015-03-16%27&sport_code=%27mlb%27&sport_code=%27win%27
class GameFeed
  include HTTParty
  base_uri 'gd2.mlb.com/components/game/mlb'

  def self.find(year,month, day)
    get("/year_#{year}/month_#{month}/day_#{day}/grid.json")
  end

end

# from: http://d3ypgbzni7egm.cloudfront.net/api/v1/game/429912/feed/live
class PlayersFeed
  include HTTParty
  base_uri 'gd2.mlb.com/components/game/mlb'

  def self.find(year,month, day, game_id)
    get("/year_#{year}/month_#{month}/day_#{day}/gid_#{game_id}/players.xml")
  end

end