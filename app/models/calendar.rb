class Calendar < ActiveRecord::Base
	has_many :replays

	extend SimpleCalendar
  #has_calendar

  # Or set a custom attribute for simple_calendar to sort by
  has_calendar :attribute => :date
	

end
