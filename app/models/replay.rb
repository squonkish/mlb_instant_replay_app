class Replay < ActiveRecord::Base
  

	validates_presence_of :date,:inning,:type_of_challenge,:outs,:challenging_team, :play_type,:challenging_team,:replay_official_ruling, :play_location, :orig_ruling_on_field,:game_start_time, :type_of_challenge
   #           , :visiting_team, :home_team, :hp_umpire,  
   #           :first_b_umpire, :second_b_umpire, :third_b_umpire,  
    #           :score, :in_favor_of,  :runners_on,  
     #         :calling_umpire, :play_desc,  :headset_ump1, :headset_ump2, :orig_ruling_on_field, 
      #        :replay_official_ruling, :placement_of_runners, :was_ruling_correct, :play_location, :stadium, :admin, 
        #      :replay_tech, :definitive_angle, :cameras_available, :notes, :video_link, :screenshot_link


  
  $new_replay = Replay.new
  $new_email = Email.new


  


  def self.to_csv(options ={})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |replay|
        csv << replay.attributes.values_at(*column_names)
      end
    end
  end
  
  
end

# from: http://www.mlb.com/lookup/named.schedule_vw.bam?season=2015&game_date=%272015-03-16%27&sport_code=%27mlb%27&sport_code=%27win%27
class GameFeed
  include HTTParty
  base_uri 'gd2.mlb.com/components/game/mlb'

  def self.find(year,month, day)
    get("/year_#{year}/month_#{month}/day_#{day}/grid.json")
  end

end

# from: http://d3ypgbzni7egm.cloudfront.net/api/v1/game/429912/feed/live
class PlayersFeed
  include HTTParty
  base_uri 'gd2.mlb.com/components/game/mlb'

  def self.find(year,month, day, game_id)
    get("/year_#{year}/month_#{month}/day_#{day}/gid_#{game_id}/players.xml")
  end

end