class UserMailer < ApplicationMailer
	default from: 'andrew@squonklabs.com'
 
  def welcome_email(user)
    @user = user
    @url  = 'http://squonklabs.com'
    mail(to: @user.email, subject: 'test email')
  end
end
