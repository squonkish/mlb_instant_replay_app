class ApplicationMailer < ActionMailer::Base
  default from: "andrew@squonklabs.com"
  layout 'mailer'
end
