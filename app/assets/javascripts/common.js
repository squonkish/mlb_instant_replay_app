var ready = function() {


    // EMAIL GENERATOR FORMS

    hide_forms();
    moment.tz.add('Pacific/Noumea|NCT|80 70|0101|1Lzm0 1zb0 Op0');
    var stamp = moment().tz("Pacific/Noumea");
    
    var json_link = 'http://gd2.mlb.com/components/game/mlb/year_' + stamp.format('YYYY') + '/month_' + stamp.format('MM') + '/day_' + stamp.format('DD') + '/grid.json';
    var xml_link_pre = 'http://gd2.mlb.com/components/game/mlb/year_' + stamp.format('YYYY') + '/month_' + stamp.format('MM') + '/day_' + stamp.format('DD') + '/gid_' + stamp.format('YYYY') + '_' + stamp.format('MM') + '_' + stamp.format('DD') + '_';
   

    function hide_forms() {
        $('#replay_homerun_variation').hide();
        $('#replay_tag_play_variation').hide();
        $('#replay_force_play_variation').hide();
        $('#replay_catch_no_catch_variation').hide();
        $('#replay_hit_by_pitch_variation').hide();
        $('#replay_fair_foul_variation').hide();
        $('#replay_violation_of_collision_variation').hide();
        $('#replay_fan_interference_variation').hide();
        $('.generator .new_replay').hide();
    }

    function show_form(name) {
        $('form.new_replay#' + name).show();
    }

    function is_new_replay() {
        if ($('.date_select_new_replay').length == 1) {
            return true;
        } else {
            return false;
        }
    }
    $(".date_select_new_replay").prepend("<option value=''>Select Game</option>");

    function select_html(class_name, html_content) {
        $('.' + class_name).each(function() {
            $(this).html(html_content);
            // $(this).html('<option value="">N/A</option>'+html_content);
        });
    }

    function value_html(class_name, html_content) {
            jQuery('.' + class_name).each(function() {
                $(this).val(html_content);
            });
        }
        // onload get input selected saved from DB
    select_html('date_select', jQuery('#game_select').html());



    $('.replay_home_team_select, .replay_visiting_team_select').on('change', function() {

        on_team_change($(this).closest('form').attr('id'));

    });


    function on_team_change(this_team) {
        var home_team_select = $('#' + this_team).find('.replay_home_team_select').val();
        var visiting_team_select = $('#' + this_team).find('.replay_visiting_team_select').val();

        var count = get_data_count('.date_select');

        if ((window.element_ee.data.games.game[count].home_name_abbrev == home_team_select) || (window.element_ee.data.games.game[count].away_name_abbrev == visiting_team_select)) {

            value_html('replay_visiting_team', window.element_ee.data.games.game[count].away_name_abbrev);
            value_html('replay_home_team', window.element_ee.data.games.game[count].home_name_abbrev);
            value_html('replay_home_team_2', window.element_ee.data.games.game[count].home_name_abbrev);


            select_html('replay_visiting_team_player_name_2', window.offense_players);
            select_html('replay_visiting_team_player_name', window.offense_players);
            select_html('replay_home_team_player_name', window.defense_players);
            select_html('replay_home_team_player_name2', window.defense_players);
            select_html('replay_home_team_2_select', window.defense_players);

        } else {


            value_html('replay_visiting_team', window.element_ee.data.games.game[count].home_name_abbrev);
            value_html('replay_home_team', window.element_ee.data.games.game[count].away_name_abbrev);
            value_html('replay_home_team_2', window.element_ee.data.games.game[count].away_name_abbrev);


            select_html('replay_visiting_team_player_name_2', window.defense_players);
            select_html('replay_visiting_team_player_name', window.defense_players);
            select_html('replay_home_team_player_name', window.offense_players);
            select_html('replay_home_team_player_name2', window.offense_players);
            select_html('replay_home_team_2_select', window.offense_players);
        }

        // .replay_home_team_player_name2
        // .replay_home_team_player_name
        // .replay_visiting_team_player_name_2
        // .replay_visiting_team_player_name
        // replay_home_team_2
        // replay_home_team_2_select
        // console.log(window.offense_players);
        if (this_team == 'form_hit_by_pitch') {
			
		if ((window.element_ee.data.games.game[count].home_name_abbrev == home_team_select) || (window.element_ee.data.games.game[count].away_name_abbrev == visiting_team_select)) {
// the actual defence from the list is the defence.
			
                    $.ajax({
                        type: 'GET',
                        url: xml_link,
                        dataType: "xml",
                        success: function(xml) {

                            var pickoff_team1 = '<option value="">N/A</option>';
                            var pickoff_team2 = '<option value="">N/A</option>';
                            $(xml).find('team').each(function() {

                                if ($(this).attr('type') == 'home') {

                                    $(this).find('player').each(function() {

                                        if ($(this).attr('position') == 'P') {
                                        pickoff_team1 += '<option value="' + jQuery(this).attr('position') + " " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '" >' + jQuery(this).attr('position') + ". " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '</option>';
										}
                                    });
                                }
                                if ($(this).attr('type') == 'away') {

                                    $(this).find('player').each(function() {

                                            pickoff_team2 += '<option value="' + jQuery(this).attr('position') + " " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '" >' + jQuery(this).attr('position') + ". " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '</option>';

                                    });
                                }
                            });

                            value_html('replay_visiting_team', window.element_ee.data.games.game[count].away_name_abbrev);
                            value_html('replay_home_team', window.element_ee.data.games.game[count].home_name_abbrev);
                            value_html('replay_home_team_2', window.element_ee.data.games.game[count].home_name_abbrev);

                            select_html('replay_visiting_team_player_name_2', pickoff_team2);
                            select_html('replay_visiting_team_player_name', pickoff_team2);
                            select_html('replay_home_team_player_name', pickoff_team1);
                            select_html('replay_home_team_player_name2', pickoff_team1);
                            select_html('replay_home_team_2_select', pickoff_team1);
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            $('#replay_third_b_umpire').val('');
                            $('#replay_second_b_umpire').val('');
                            $('#replay_first_b_umpire').val('');
                            $('#replay_hp_umpire').val('');
                        }
                    });

        } else {
//the opposite.


                    $.ajax({
                        type: 'GET',
                        url: xml_link,
                        dataType: "xml",
                        success: function(xml) {

                            var pickoff_team1 = '<option value="">N/A</option>';
                            var pickoff_team2 = '<option value="">N/A</option>';
                            $(xml).find('team').each(function() {

                                if ($(this).attr('type') == 'home') {

                                    $(this).find('player').each(function() {

                                        pickoff_team1 += '<option value="' + jQuery(this).attr('position') + " " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '" >' + jQuery(this).attr('position') + ". " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '</option>';

                                    });
                                }
                                if ($(this).attr('type') == 'away') {

                                    $(this).find('player').each(function() {
                                        if ($(this).attr('position') == 'P') {

                                            pickoff_team2 += '<option value="' + jQuery(this).attr('position') + " " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '" >' + jQuery(this).attr('position') + ". " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '</option>';

                                        }
                                    });
                                }
                            });

                            value_html('replay_visiting_team', window.element_ee.data.games.game[count].home_name_abbrev);
                            value_html('replay_home_team', window.element_ee.data.games.game[count].away_name_abbrev);
                            value_html('replay_home_team_2', window.element_ee.data.games.game[count].away_name_abbrev);


                            select_html('replay_visiting_team_player_name_2', pickoff_team1);
                            select_html('replay_visiting_team_player_name', pickoff_team1);
                            select_html('replay_home_team_player_name', pickoff_team2);
                            select_html('replay_home_team_player_name2', pickoff_team2);
                            select_html('replay_home_team_2_select', pickoff_team2);
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            $('#replay_third_b_umpire').val('');
                            $('#replay_second_b_umpire').val('');
                            $('#replay_first_b_umpire').val('');
                            $('#replay_hp_umpire').val('');
                        }
                    });
        }
			
		}
        if (this_team == 'form_tag_play') {
            if ($('#replay_tag_play_variation').val() == 'pickoff') {


                if (window.element_ee.data.games.game[count].home_name_abbrev == home_team_select) {

                    value_html('replay_visiting_team', window.element_ee.data.games.game[count].away_name_abbrev);
                    value_html('replay_home_team', window.element_ee.data.games.game[count].home_name_abbrev);
                    value_html('replay_home_team_2', window.element_ee.data.games.game[count].home_name_abbrev);


                    select_html('replay_visiting_team_player_name_2', window.offense_players);

                    select_html('replay_visiting_team_player_name', window.offense_players);
                    select_html('replay_home_team_player_name', window.defense_players);
                    select_html('replay_home_team_player_name2', window.defense_players);
                    select_html('replay_home_team_2_select', window.defense_players);

                } else {

                    $.ajax({
                        type: 'GET',
                        url: xml_link,
                        dataType: "xml",
                        success: function(xml) {

                            var pickoff_team1 = '<option value="">N/A</option>';
                            var pickoff_team2 = '<option value="">N/A</option>';
                            $(xml).find('team').each(function() {

                                if ($(this).attr('type') == 'home') {

                                    $(this).find('player').each(function() {

                                        pickoff_team1 += '<option value="' + jQuery(this).attr('position') + " " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '" >' + jQuery(this).attr('position') + ". " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '</option>';

                                    });
                                }
                                if ($(this).attr('type') == 'away') {

                                    $(this).find('player').each(function() {
                                        if ($(this).attr('position') == 'P') {

                                            pickoff_team2 += '<option value="' + jQuery(this).attr('position') + " " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '" >' + jQuery(this).attr('position') + ". " + jQuery(this).attr('first') + " " + jQuery(this).attr('last') + '</option>';

                                        }
                                    });
                                }
                            });

                            value_html('replay_visiting_team', window.element_ee.data.games.game[count].home_name_abbrev);
                            value_html('replay_home_team', window.element_ee.data.games.game[count].away_name_abbrev);
                            value_html('replay_home_team_2', window.element_ee.data.games.game[count].away_name_abbrev);


                            select_html('replay_visiting_team_player_name_2', pickoff_team1);
                            select_html('replay_visiting_team_player_name', pickoff_team1);
                            select_html('replay_home_team_player_name', pickoff_team2);
                            select_html('replay_home_team_player_name2', pickoff_team2);
                            select_html('replay_home_team_2_select', pickoff_team2);
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            $('#replay_third_b_umpire').val('');
                            $('#replay_second_b_umpire').val('');
                            $('#replay_first_b_umpire').val('');
                            $('#replay_hp_umpire').val('');
                        }
                    });
                }


            } else {
				
				if ($('#replay_tag_play_variation').val() == 'pickoff') {
					var home_team_select = $('#' + this_team).find('#var_pickoff .replay_home_team_select').val();
					var visiting_team_select = $('#' + this_team).find('#var_pickoff .replay_visiting_team_select').val();
				}
				if ($('#replay_tag_play_variation').val() == 'runner loses contact with base') {
					var home_team_select = $('#' + this_team).find('#var_runner_loses_contact .replay_home_team_select').val();
					// var visiting_team_select = $('#' + this_team).find('#var_runner_loses_contact .replay_visiting_team_select').val();
				}
				if ($('#replay_tag_play_variation').val() == 'stolen base') {
					// var home_team_select = $('#' + this_team).find('#var_steal .replay_home_team_select').val();
					var visiting_team_select = $('#' + this_team).find('#var_steal .replay_visiting_team_select').val();
				}
				if ($('#replay_tag_play_variation').val() == 'batter: attempted extra bases') {
					// var home_team_select = $('#' + this_team).find('#var_batter_attempts_extra .replay_home_team_select').val();
					var visiting_team_select = $('#' + this_team).find('#var_batter_attempts_extra .replay_visiting_team_select').val();
				}
				
				if ($('#replay_tag_play_variation').val() == 'runner: advance between bases') {
					// var home_team_select = $('#' + this_team).find('#var_runner_advance .replay_home_team_select').val();
					var visiting_team_select = $('#' + this_team).find('#var_runner_advance .replay_visiting_team_select').val();
				}
				if ($('#replay_tag_play_variation').val() == 'tag-up: attempt to advance') {
					// var home_team_select = $('#' + this_team).find('#var_tag_up .replay_home_team_select').val();
					var visiting_team_select = $('#' + this_team).find('#var_tag_up .replay_visiting_team_select').val();
				}
				if ($('#replay_tag_play_variation').val() == 'errant throw') {
					// var home_team_select = $('#' + this_team).find('#var_errant_throw .replay_home_team_select').val();
					var visiting_team_select = $('#' + this_team).find('#var_errant_throw .replay_visiting_team_select').val();
				}
				
		var if_true = (window.element_ee.data.games.game[count].away_name_abbrev == visiting_team_select);
		
				if ($('#replay_tag_play_variation').val() == 'runner loses contact with base') {
					var home_team_select = $('#' + this_team).find('#var_runner_loses_contact .replay_home_team_select').val();
					// var visiting_team_select = $('#' + this_team).find('#var_runner_loses_contact .replay_visiting_team_select').val();
					if_true = (window.element_ee.data.games.game[count].away_name_abbrev != home_team_select);
				}
                if (if_true) {
				
                    value_html('replay_visiting_team', window.element_ee.data.games.game[count].away_name_abbrev);
                    value_html('replay_home_team', window.element_ee.data.games.game[count].home_name_abbrev);
                    value_html('replay_home_team_2', window.element_ee.data.games.game[count].home_name_abbrev);


                    select_html('replay_visiting_team_player_name_2', window.offense_players);
                    select_html('replay_visiting_team_player_name', window.offense_players);
                    select_html('replay_home_team_player_name', window.defense_players);
                    select_html('replay_home_team_player_name2', window.defense_players);
                    select_html('replay_home_team_2_select', window.defense_players);

                } else {

				console.log('switched2');

                    value_html('replay_visiting_team', window.element_ee.data.games.game[count].home_name_abbrev);
                    value_html('replay_home_team', window.element_ee.data.games.game[count].away_name_abbrev);
                    value_html('replay_home_team_2', window.element_ee.data.games.game[count].away_name_abbrev);


                    select_html('replay_visiting_team_player_name_2', window.defense_players);
                    select_html('replay_visiting_team_player_name', window.defense_players);
                    select_html('replay_home_team_player_name', window.offense_players);
                    select_html('replay_home_team_player_name2', window.offense_players);
                    select_html('replay_home_team_2_select', window.offense_players);
                }
            }
        }

    }


    function get_data_count(varPara) {
        window.data_count = 0;
        var data_original = jQuery(varPara + ' option:selected').val();
        var count_in = 0;

        if (jQuery('.date_select_new_replay').length == 1) {
            count_in = -1
            data_original = jQuery('.date_select_new_replay option:selected').val();
            jQuery('.date_select_new_replay option').each(function() {
                if (jQuery(this).val() == data_original) {
                    window.data_count = count_in;
                    return false;
                } else {
                    count_in++;
                }
            });
        } else {
            var index, len;
            len = window.element_ee.data.games.game.length;
            for (index = 0; index < len; ++index) {
                if (window.element_ee.data.games.game[index].id == data_original) {
                    window.data_count = index;
                    return window.data_count;
                }
            }
        }
        return window.data_count;
    }

    var playtype_load = $('#replay_play_type').val()

    if (playtype_load == 'force play') {

        show_form('form_force_play');
        $('#replay_force_play_variation').show();

    } else if (playtype_load == 'home run') {

        show_form('form_homerun');
        $('#replay_homerun_variation').show();

    } else if (playtype_load == 'tag play') {
        show_form('form_tag_play');
        $('#replay_tag_play_variation').show();

    } else if (playtype_load == 'catch / no catch') {
        show_form('form_catch_nocatch');
        $('#replay_catch_no_catch_variation').show();

    } else if (playtype_load == 'fair / foul') {
        show_form('form_fair_foul');
        $('#replay_fair_foul_variation').show();

    } else if (playtype_load == 'violation of collision at home plate') {
        show_form('form_violation_of_collision');
        $('#replay_violation_of_collision_variation').show();

    } else if (playtype_load == 'hit by pitch') {
        show_form('form_hit_by_pitch');
        $('#replay_hit_by_pitch_variation').show();

    } else if (playtype_load == 'fan interference') {
        show_form('form_fan_interference');
        $('#replay_fan_interference_variation').show();
    } 

    $('.replay_play_type').val($('#replay_play_type').val());


    $.ajax({
        type: 'GET',
        url: json_link,
        dataType: 'json',
        success: function(element_ee) {
            $count = 0;
            window.element_ee = element_ee;

            var count = get_data_count('#game_select');
            //value_html('replay_inning',element_ee.data.games.game[count].inning);
            /*    value_html('replay_visiting_team', element_ee.data.games.game[count].away_team_name);
               value_html('replay_home_team', element_ee.data.games.game[count].home_name_abbrev);
               value_html('replay_venue', element_ee.data.games.game[count].venue);
               value_html('replay_game_start_time', element_ee.data.games.game[count].event_time);
               $('#replay_stadium').val(element_ee.data.games.game[count].venue); */
        }
    });
    $('#replay_play_type').on('change', function() {
        hide_forms();
        var options_call = '';
        var options_call_replay = '<option value=""></option>';
        options_call == '<option value="Home run" >Home run</option>';
        select_html('replay_orig_ruling_on_field', options_call);

            /// only Crew Chief Review for homerun
            if ($(this).val() == 'home run') {

                if (is_new_replay()) {
                    $("#replay_type_of_challenge option:eq(2)").hide()
            
                }
                
            }else{
                $("#replay_type_of_challenge option:eq(2)").show()
            }
            ///

        if ($(this).val() == 'force play') {

            show_form('form_force_play');
            $('#replay_force_play_variation').show();

            if (is_new_replay()) {
                options_call_replay += '<option value="safe">safe</option>';
                options_call_replay += '<option value="out">out</option>';
            }


        } else if ($(this).val() == 'home run') {

            show_form('form_homerun');
            $('#replay_homerun_variation').show();

            if (is_new_replay()) {
                options_call_replay += '<option value="a home run">a home run</option>';
                options_call_replay += '<option value=in play>in play</option>';
                
                //$("#replay_type_of_challenge option:eq(2)").hide()
                //console.log(f)
            }
            

        } else if ($(this).val() == 'tag play') {

            show_form('form_tag_play');
            $('#replay_tag_play_variation').show();

            if (is_new_replay()) {
                options_call_replay += '<option value="safe">safe</option>';
                options_call_replay += '<option value="out">out</option>';
            }

        } else if ($(this).val() == 'catch / no catch') {
            show_form('form_catch_nocatch');
            $('#replay_catch_no_catch_variation').show();

            if (is_new_replay()) {
                options_call_replay += '<option value="catch">catch</option>';
                options_call_replay += '<option value="no catch">no catch</option>';
            }

        } else if ($(this).val() == 'fair / foul') {
            show_form('form_fair_foul');
            $('#replay_fair_foul_variation').show();

            if (is_new_replay()) {
                options_call_replay += '<option value="fair">fair</option>';
                options_call_replay += '<option value="foul">foul</option>';
            }

        } else if ($(this).val() == 'violation of collision at home plate') {
            show_form('form_violation_of_collision');
            if (is_new_replay()) {
                options_call_replay += '<option value="violation of collision at home plate">violation of collision at home plate</option>';
                options_call_replay += '<option value="no violation of collision at home plate">no violation of collision at home plate</option>';
            }

        } else if ($(this).val() == 'hit by pitch') {
            show_form('form_hit_by_pitch');
            if (is_new_replay()) {
                options_call_replay += '<option value="hit by pitch">hit by pitch</option>';
                options_call_replay += '<option value="not hit by pitch">not hit by pitch</option>';
            }

        } else if ($(this).val() == 'custom') {

           if (is_new_replay()) {
                options_call_replay += '<option value="custom">custom</option>';
             }

        } else if ($(this).val() == 'fan interference') {

           if (is_new_replay()) {
                options_call_replay += '<option value="in play">in play</option>';
                options_call_replay += '<option value="foul">foul</option>';
             }

        show_form('form_fan_interference');
        $('#replay_fan_interference_variation').show();
    }
        $('.replay_play_type').val($('#replay_play_type').val());

        if (is_new_replay()) {
            $('#replay_orig_ruling_on_field').html(options_call_replay);
        }

    });


    $('#replay_homerun_variation').on('change', function() {
        var options_call = '';
        if ($(this).val() == 'stadium boundaries') {
            options_call += '<option value="a home run" >a home run</option>';
            options_call += '<option value="in play" >in play</option>';
        } else if ($(this).val() == 'fair / foul') {
            options_call += '<option value="home run" >home run</option>';
            options_call += '<option value="foul ball" >foul ball</option>';
        } else if ($(this).val() == 'fan - interference') {
            options_call += '<option value="a home run" >a home run</option>';
            options_call += '<option value="fan - interference" >fan - interference</option>';
        }
        select_html('replay_callonfield', options_call);
        select_html('replay_callonfield_after', options_call);
    });

    $('#replay_tag_play_variation').on('change', function() {

        if ($(this).val() == 'pickoff') {
            $('#var_pickoff').show();
            $('#var_steal').hide();
            $('#var_runner_loses_contact').hide();
            $('#var_batter_attempts_extra').hide();
            $('#var_runner_advance').hide();
            $('#var_tag_up').hide();
            $('#var_errant_throw').hide();
        } else if ($(this).val() == 'stolen base') {
            $('#var_pickoff').hide();
            $('#var_steal').show();
            $('#var_runner_loses_contact').hide();
            $('#var_batter_attempts_extra').hide();
            $('#var_runner_advance').hide();
            $('#var_tag_up').hide();
            $('#var_errant_throw').hide();
        }
        else if ($(this).val() == 'runner loses contact with base') {
            $('#var_pickoff').hide();
            $('#var_steal').hide();
            $('#var_runner_loses_contact').show();
            $('#var_batter_attempts_extra').hide();
            $('#var_runner_advance').hide();
            $('#var_tag_up').hide();
            $('#var_errant_throw').hide();
        }
        else if ($(this).val() == 'batter: attempted extra bases') {
            $('#var_pickoff').hide();
            $('#var_steal').hide();
            $('#var_runner_loses_contact').hide();
            $('#var_batter_attempts_extra').show();
            $('#var_runner_advance').hide();
            $('#var_tag_up').hide();
            $('#var_errant_throw').hide();

            //$('#var_batter_attempts_extra .replay_visiting_team_select').on('change', function () {
              //  $('..batter_attempts_extra_def_team').html($('#var_transfer_at_2B .replay_visiting_team_select :selected').val()) ;
            //});

        }
        else if ($(this).val() == 'runner: advance between bases') {
            $('#var_pickoff').hide();
            $('#var_steal').hide();
            $('#var_runner_loses_contact').hide();
            $('#var_batter_attempts_extra').hide();
            $('#var_runner_advance').show();
            $('#var_tag_up').hide();
            $('#var_errant_throw').hide();
        }
        else if ($(this).val() == 'tag-up: attempt to advance') {
            $('#var_pickoff').hide();
            $('#var_steal').hide();
            $('#var_runner_loses_contact').hide();
            $('#var_batter_attempts_extra').hide();
            $('#var_runner_advance').hide();
            $('#var_tag_up').show();
            $('#var_errant_throw').hide();
        }
        else if ($(this).val() == 'errant throw') {
            $('#var_pickoff').hide();
            $('#var_steal').hide();
            $('#var_runner_loses_contact').hide();
            $('#var_batter_attempts_extra').hide();
            $('#var_runner_advance').hide();
            $('#var_tag_up').hide();
            $('#var_errant_throw').show();
        }
    });

    $('#replay_force_play_variation').on('change', function() {

        if ($(this).val() == 'timing') {
            $('#var_timing').show();
            $('#var_fielder').hide();
            $('#var_double_play').hide();
        } else if ($(this).val() == 'fielder maintaining contact w/bag') {
            $('#var_timing').hide();
            $('#var_fielder').show();
            $('#var_double_play').hide();
        } else if($(this).val() == 'double play: call at 1B'){
            $('#var_timing').hide();
            $('#var_fielder').hide();
            $('#var_double_play').show();

        }
    });

    $('#var_double_play .replay_home_team_player_name2').on('change', function () {
        $('.defensive_player_2_double_play').html($('.replay_home_team_player_name2 :selected').val()) ;
    });

    $('#replay_fair_foul_variation').on('change', function() {

        if ($(this).val() == 'outfield') {
            $('#var_ff_outfield').show();
        } 
    });


    $('#replay_homerun_variation').on('change', function() {

        if ($(this).val() == 'fair / foul') {
            $('#var_homerun_fair_foul').show();
            $('#var_homerun_fan_interference').hide();
            $('#var_stadium_boundaries').hide();
        } else if ($(this).val() == 'fan - interference') {
            $('#var_homerun_fair_foul').hide();
            $('#var_homerun_fan_interference').show();
            $('#var_stadium_boundaries').hide();
        } else if($(this).val() == 'stadium boundaries'){
            $('#var_homerun_fair_foul').hide();
            $('#var_homerun_fan_interference').hide();
            $('#var_stadium_boundaries').show();
        }
    });

    //populate runner text from selection
    $('#var_transfer_at_2B .replay_visiting_team_player_name').on('change', function () {
                $('.transfer_2b_runner').html($('#var_transfer_at_2B .replay_visiting_team_player_name :selected').val()) ;
            });
    $('#var_transfer_at_2B .replay_visiting_team_select').on('change', function () {
                $('.transfer_2b_team').html($('#var_transfer_at_2B .replay_visiting_team_select :selected').val()) ;
            });
    ///

    // hit by pitch
    $('#form_hit_by_pitch .replay_home_team_select').on('change', function () {
                $('.hit_by_pitch_inside_to').html($('#form_hit_by_pitch .replay_visiting_team_select').val()) ;
            });

    // tag-up
    $('#var_tag_up .replay_visiting_team_select').on('change', function () {
                $('.tag_up_visiting_team').html($('#var_tag_up .replay_visiting_team_select :selected').val()) ;
            });

    $('#replay_catch_no_catch_variation').on('change', function() {
        var options_call= '';
        
        if ($(this).val() == 'transfer at 2B') {

            var options_call = '';
                options_call += '<option value="" ></option>';
                options_call += '<option value="safe" >safe</option>';
                options_call += '<option value="out" >out</option>';
                if(is_new_replay()){
     $('#replay_orig_ruling_on_field').html(options_call);
    }
                select_html('replay_callonfield', options_call);
                select_html('replay_callonfield_after', options_call);
            $('#var_transfer_at_2B').show();
            $('#var_outfield_transfer').hide();
            $('#var_outfield_ball_off_wall').hide();
            $('#var_outfield_trap_play').hide();
        } else if ($(this).val() == 'outfield: transfer') {
            var options_call = '';
                options_call += '<option value="" ></option>';
                options_call += '<option value="catch" >catch</option>';
                options_call += '<option value="no catch" >no catch</option>';
                if(is_new_replay()){
     $('#replay_orig_ruling_on_field').html(options_call);
    }
                select_html('replay_callonfield', options_call);
                select_html('replay_callonfield_after', options_call);
            $('#var_transfer_at_2B').hide();
            $('#var_outfield_transfer').show();
            $('#var_outfield_ball_off_wall').hide();
            $('#var_outfield_trap_play').hide();
        } else if($(this).val() == 'outfield: ball off wall'){
            var options_call = '';
                options_call += '<option value="" ></option>';
                options_call += '<option value="catch" >catch</option>';
                options_call += '<option value="no catch" >no catch</option>';
                if(is_new_replay()){
     $('#replay_orig_ruling_on_field').html(options_call);
    }
                select_html('replay_callonfield', options_call);
                select_html('replay_callonfield_after', options_call);
            $('#var_transfer_at_2B').hide();
            $('#var_outfield_transfer').hide();
            $('#var_outfield_ball_off_wall').show();
            $('#var_outfield_trap_play').hide();
        } else if($(this).val() == 'outfield: trap play'){
            var options_call = '';
                options_call += '<option value="" ></option>';
                options_call += '<option value="catch" >catch</option>';
                options_call += '<option value="no catch" >no catch</option>';
                if(is_new_replay()){
     $('#replay_orig_ruling_on_field').html(options_call);
    }
                select_html('replay_callonfield', options_call);
                select_html('replay_callonfield_after', options_call);
            $('#var_transfer_at_2B').hide();
            $('#var_outfield_transfer').hide();
            $('#var_outfield_ball_off_wall').hide();
            $('#var_outfield_trap_play').show();
        }
    });

    $('#replay_fan_interference_variation').on('change', function() {

        if ($(this).val() == 'foul ball') {
            $('#var_fi_foul_ball').show();
            $('#var_fi_bounding_ball').hide();
            var options_call = '';
                options_call += '<option value="no fan interference" >no fan interference</option>';
                options_call += '<option value="fan interference" >fan interference</option>';
                if(is_new_replay()){
     $('#replay_orig_ruling_on_field').html(options_call);
    }
                select_html('replay_callonfield', options_call);
                select_html('replay_callonfield_after', options_call);
        } 
        //boudning ball
        else if ($(this).val() == 'bounding ball') {
            var options_call = '';
                options_call += '<option value="in play" >in play</option>';
                options_call += '<option value="foul" >foul</option>';
                if(is_new_replay()){
     $('#replay_orig_ruling_on_field').html(options_call);
    }
                select_html('replay_callonfield', options_call);
                select_html('replay_callonfield_after', options_call);
        } 
            $('#var_fi_foul_ball').hide();
            $('#var_fi_bounding_ball').show();
            
    });

    // on CALLAFTER
    $('.replay_callonfield_after').on('change', function() {

        var OUTCOME = '';
        var playtype_id = $(this).closest('form').attr('id');
        if ($(this).val() == $('#' + playtype_id).find('.replay_callonfield').val()) {

            OUTCOME += '<option value="Confirmed" >Confirmed</option>';
            OUTCOME += '<option value="Stands" >Stands</option>';

        } else {
            OUTCOME += '<option value="Overturned" >Overturned</option>';
        }

        $('#' + playtype_id).find('.replay_replay_official_ruling').html(OUTCOME);
        $('#' + playtype_id).find('.replay_replay_official_ruling2').html(OUTCOME);


    });

    // call 'is' remove for stands
    
        $('.replay_replay_official_ruling').on('change', function() {
            if ($(this).val() != "Confirmed") {
                var playtype_id = $(this).closest('form').attr('id');
                $('#' + playtype_id).find('.is').hide();
            }else{
                var playtype_id = $(this).closest('form').attr('id');
                $('#' + playtype_id).find('.is').show();
            };
            
        })
    
    //
    $('.date_select').on('change', function() {
        var playtype_id = $(this).closest('form').attr('id');
        on_body_load('#' + playtype_id + ' .date_select');
    });
    // IF selected OUTS >= 0.  OUTSAFTER option can only be greater than
    $('.replay_outs').on('change', function() {
        if ($(this).val() == '0') {
            $('.replay_outsafter').html('<option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option>');
        } else if ($(this).val() == '1') {
            $('.replay_outsafter').html('<option value="1">1</option><option value="2">2</option><option value="3">3</option>');
        } else if ($(this).val() == '2') {
            $('.replay_outsafter').html('<option value="2">2</option><option value="3">3</option>');
        } else if ($(this).val() == '3') {
            $('.replay_outsafter').html('<option value="3">3</option>');
        }
    });
    // IF selected OUTSAFTER = 3.  RUNNERSAFTER option can only be “End of Inning”
    $('.replay_outsafter').on('change', function() {
        if ($(this).val() == '3') {
            $('.replay_runners_on').html('<option value="End of Inning">End of Inning</option>');
        } else {
            $('.replay_runners_on').html('<option value="no runners on">no runners on</option><option value="runner on 1B">runner on 1B</option><option value="runner on 2B">runner on 2B</option><option value="runner on 3B">runner on 3B</option><option value="runners on 1B and 2B">runners on 1B and 2B</option><option value="runners on 1B and 3B">runners on 1B and 3B</option><option value="runners on 2B and 3B">runners on 2B and 3B</option><option value="bases loaded">bases loaded</option>');
        }
    });

    // on GAME select in NEW REPLAY.
    on_body_load()
    value_html('date_select', $('#game_select').val());

    function on_body_load(optionalArg) {
            var yes_load = optionalArg;
            var optionalArg = (typeof optionalArg === 'undefined') ? '#game_select' : optionalArg;


            if ($('.date_select_new_replay').length == 1) {
                optionalArg = ".date_select_new_replay";
            }

            if (!$('.date_select').val() || $('.date_select').val().length == 0 || $('.date_select').val() == '') {
                return false;
            }

            // load FORCE play variation
            if ($('#replay_force_play_variation :selected').val() == 'timing') {
                $('#var_timing').show();
                $('#var_fielder').hide();
                $('#var_double_play').hide();
            } else if ($('#replay_force_play_variation :selected').val() == 'fielder maintaining contact w/bag') {
                $('#var_timing').hide();
                $('#var_fielder').show();
                $('#var_double_play').hide();
            } else if ($('#replay_force_play_variation :selected').val() == 'double play: call at 1B') {
                $('#var_timing').hide();
                $('#var_fielder').hide();
                $('#var_double_play').show();
            }


            // load HOMERUN variation
            if ($('#replay_homerun_variation :selected').val() == 'fair / foul') {
                $('#var_homerun_fair_foul').show();
                $('#var_homerun_fan_interference').hide();
                $('#var_stadium_boundaries').hide();
                var options_call = '';
                options_call += '<option value="fair home run" >a home run</option>';
                options_call += '<option value="foul ball - no home run" >foul ball - no home run</option>';
                select_html('replay_callonfield', options_call);
                select_html('replay_callonfield_after', options_call);

            } else if ($('#replay_homerun_variation :selected').val() == 'fan - interference') {
                $('#var_homerun_fair_foul').hide();
                $('#var_homerun_fan_interference').show();
                $('#var_stadium_boundaries').hide();
                var options_call = '';
                options_call += '<option value="a home run" >a home run</option>';
                options_call += '<option value="fan - interference" >fan - interference</option>';
                select_html('replay_callonfield', options_call);
                select_html('replay_callonfield_after', options_call);

            } else if ($('#replay_homerun_variation :selected').val() == 'stadium boundaries') {
                $('#var_homerun_fair_foul').hide();
                $('#var_homerun_fan_interference').hide();
                $('#var_stadium_boundaries').show();
                var options_call = '';
                options_call += '<option value="in play" >in play</option>';
                options_call += '<option value="a home run" >a home run</option>';
                select_html('replay_callonfield', options_call);
                select_html('replay_callonfield_after', options_call);
            }


            // load TAG play variation
            if ($('#replay_tag_play_variation :selected').val() == 'pickoff') {
                $('#var_pickoff').show();
                $('#var_steal').hide();
                $('#var_runner_loses_contact').hide();
                $('#var_batter_attempts_extra').hide();
                $('#var_runner_advance').hide();
                $('#var_tag_up').hide();
                $('#var_errant_throw').hide();

            } else if ($('#replay_tag_play_variation :selected').val() == 'stolen base') {
                $('#var_pickoff').hide();
                $('#var_steal').show();
                $('#var_runner_loses_contact').hide();
                $('#var_batter_attempts_extra').hide();
                $('#var_runner_advance').hide();
                $('#var_tag_up').hide();
                $('#var_errant_throw').hide();
            }
            else if ($('#replay_tag_play_variation :selected').val() == 'runner loses contact with base') {
                $('#var_pickoff').hide();
                $('#var_steal').hide();
                $('#var_runner_loses_contact').show();
                $('#var_batter_attempts_extra').hide();
                $('#var_runner_advance').hide();
                $('#var_tag_up').hide();
                $('#var_errant_throw').hide();
            }
            else if ($('#replay_tag_play_variation :selected').val() == 'batter: attempted extra bases') {
                $('#var_pickoff').hide();
                $('#var_steal').hide();
                $('#var_runner_loses_contact').hide();
                $('#var_batter_attempts_extra').show();
                $('#var_runner_advance').hide();
                $('#var_tag_up').hide();
                $('#var_errant_throw').hide();
            }
            else if ($('#replay_tag_play_variation :selected').val() == 'runner: advance between bases') {
                $('#var_pickoff').hide();
                $('#var_steal').hide();
                $('#var_runner_loses_contact').hide();
                $('#var_batter_attempts_extra').hide();
                $('#var_runner_advance').show();
                $('#var_tag_up').hide();
                $('#var_errant_throw').hide();
            }
            else if ($('#replay_tag_play_variation :selected').val() == 'tag-up: attempt to advance') {
                $('#var_pickoff').hide();
                $('#var_steal').hide();
                $('#var_runner_loses_contact').hide();
                $('#var_batter_attempts_extra').hide();
                $('#var_runner_advance').hide();
                $('#var_tag_up').show();
                $('#var_errant_throw').hide();
            }
            else if ($('#replay_tag_play_variation :selected').val() == 'errant throw') {
                $('#var_pickoff').hide();
                $('#var_steal').hide();
                $('#var_runner_loses_contact').hide();
                $('#var_batter_attempts_extra').hide();
                $('#var_runner_advance').hide();
                $('#var_tag_up').hide();
                $('#var_errant_throw').show();
            } else if ($('#replay_tag_play_variation :selected').val() == '') {
                $('#var_pickoff').hide();
                $('#var_steal').hide();
                $('#var_runner_loses_contact').hide();
                $('#var_batter_attempts_extra').hide();
                $('#var_runner_advance').hide();
                $('#var_tag_up').hide();
                $('#var_errant_throw').hide();
            }


            // load FAIR FOUL variation
            if ($('#replay_fair_foul_variation :selected').val() == 'outfield') {
                $('#var_ff_outfield').show();
            } else if ($('#replay_fair_foul_variation :selected').val() == '') {
                
            }

            // load Fan interference variation
            if ($('#replay_fan_interference_variation :selected').val() == 'foul ball') {
                $('#var_fi_foul_ball').show();
                $('#var_fi_bounding_ball').hide();
            } else if ($('#replay_fan_interference_variation :selected').val() == 'bounding ball') {
                $('#var_fi_foul_ball').hide();
                $('#var_fi_bounding_ball').show();
            }

            // load catch_no_catch variation
            if ($('#replay_catch_no_catch_variation :selected').val() == 'transfer at 2B') {
                $('#var_transfer_at_2B').show();
                $('#var_outfield_transfer').hide();
                $('#var_outfield_ball_off_wall').hide();
                $('#var_outfield_trap_play').hide();

            } else if ($('#replay_catch_no_catch_variation :selected').val() == 'outfield: transfer') {
                $('#var_transfer_at_2B').hide();
                $('#var_outfield_transfer').show();
                $('#var_outfield_ball_off_wall').hide();
                $('#var_outfield_trap_play').hide();

            } else if ($('#replay_catch_no_catch_variation :selected').val() == 'outfield: ball off wall') {
                $('#var_transfer_at_2B').hide();
                $('#var_outfield_transfer').hide();
                $('#var_outfield_ball_off_wall').show();
                $('#var_outfield_trap_play').hide();

            } else if ($('#replay_catch_no_catch_variation :selected').val() == 'outfield: trap play') {
                $('#var_transfer_at_2B').hide();
                $('#var_outfield_transfer').hide();
                $('#var_outfield_ball_off_wall').hide();
                $('#var_outfield_trap_play').show();

            } else if ($('#replay_catch_no_catch_variation :selected').val() == '') {
                $('#var_transfer_at_2B').hide();
                $('#var_outfield_transfer').hide();
                $('#var_outfield_ball_off_wall').hide();
                $('#var_outfield_trap_play').hide();
            };




            $.ajax({
                type: 'GET',
                url: json_link,
                dataType: 'json',
                success: function(element_ee) {
                    $count = 0;
                    window.element_ee = element_ee;

                    var count = get_data_count(optionalArg);
                    //value_html('replay_inning',element_ee.data.games.game[count].inning);
                    if (typeof yes_load !== 'undefined') {
                        value_html('replay_visiting_team', element_ee.data.games.game[count].away_name_abbrev);
                        value_html('replay_home_team', element_ee.data.games.game[count].home_name_abbrev);
                        value_html('replay_venue', element_ee.data.games.game[count].venue);
                        value_html('replay_game_start_time', element_ee.data.games.game[count].event_time);
                        $('#replay_stadium').val(element_ee.data.games.game[count].venue);
                        if ($('.date_select_new_replay').length == 1) {
                            var original = '<option></option>';
                            original += '<option value="' + element_ee.data.games.game[count].home_name_abbrev + '">' + element_ee.data.games.game[count].home_name_abbrev + '</option>';
                            original += '<option value="' + element_ee.data.games.game[count].away_name_abbrev + '">' + element_ee.data.games.game[count].away_name_abbrev + '</option>';

                            $('#replay_in_favor_of').html(original);
                            $('#replay_challenging_team').html(original);
                            //change team abbrev
                            $("#replay_challenging_team option:contains('CHC')").text(function () {
                                return $(this).text().replace("CHC", "CHI"); 
                            });
                            $("#replay_challenging_team option:contains('WSH')").text(function () {
                                return $(this).text().replace("WSH", "WAS"); 
                            });
                        }
                    }
                    //$('#replay_inning').val(element_ee.data.games.game[count].inning);
                }
            });
            xml_link = xml_link_pre + $(optionalArg).val().substr(11).replace(/-/g, "_") + '/players.xml';
            //xml_link = 'http://gd2.mlb.com/components/game/mlb/year_2015/month_06/day_03/gid_2015_06_03_minmlb_bosmlb_2/players.xml';


            $.ajax({
                type: 'GET',
                url: xml_link,
                dataType: "xml",
                success: function(xml) {
                    window.offense_players = '<option value="">N/A</option>';
                    var offense_manager = '';
                    var offense_id = '';

                    window.defense_players = '<option value="">N/A</option>';
                    var defense_manager = '';
                    var defense_id = '';

                    var whochallenged = '';

                    var team_option = '';

                    if (($('.date_select_new_replay').length == 1) && (typeof yes_load !== 'undefined')) {
                        var umpires_new = '<option value=""></option>'
                        $(xml).find('umpire').each(function() {

                            if ($(this).attr('position') == 'home') {
                                $('#replay_hp_umpire').val($(this).attr('name'));
                                umpires_new += '<option value="' + $(this).attr('name') + '">' + $(this).attr('name') + '</option>';
                            } else if ($(this).attr('position') == 'first') {
                                $('#replay_first_b_umpire').val($(this).attr('name'));
                                umpires_new += '<option value="' + $(this).attr('name') + '">' + $(this).attr('name') + '</option>';
                            } else if ($(this).attr('position') == 'second') {
                                $('#replay_second_b_umpire').val($(this).attr('name'));
                                umpires_new += '<option value="' + $(this).attr('name') + '">' + $(this).attr('name') + '</option>';
                            } else if ($(this).attr('position') == 'third') {
                                $('#replay_third_b_umpire').val($(this).attr('name'));
                                umpires_new += '<option value="' + $(this).attr('name') + '">' + $(this).attr('name') + '</option>';
                            }


                        });

                        //$('#replay_replay_official').html(umpires_new);
                        $('#replay_visiting_team_player_name').html(umpires_new);
                        $('#replay_calling_umpire').html(umpires_new);
                        $('#replay_headset_ump1').html(umpires_new);
                        $('#replay_headset_ump2').html(umpires_new);
                    }



                    var umpires_new = '<option value=""></option>'
                    $(xml).find('umpire').each(function() {

                        if ($(this).attr('position') == 'home') {
                            umpires_new += '<option value="' + $(this).attr('name') + '">' + $(this).attr('name') + '</option>';
                        } else if ($(this).attr('position') == 'first') {
                            umpires_new += '<option value="' + $(this).attr('name') + '">' + $(this).attr('name') + '</option>';
                        } else if ($(this).attr('position') == 'second') {
                            umpires_new += '<option value="' + $(this).attr('name') + '">' + $(this).attr('name') + '</option>';
                        } else if ($(this).attr('position') == 'third') {
                            umpires_new += '<option value="' + $(this).attr('name') + '">' + $(this).attr('name') + '</option>';
                        }


                    });
                    select_html('replay_replay_official', umpires_new);
                    select_html('replay_calling_umpire', umpires_new);

                    $(xml).find('team').each(function() {

                        team_option += '<option value="' + $(this).attr('id') + '">' + $(this).attr('name') + '</option>';
                        if ($(this).attr('type') == 'away') {
                            offense_id = $(this).attr('id') + ' Manager ';

                            $(this).find('player').each(function() {

                                offense_players += '<option value="' + $(this).attr('position') + " " + $(this).attr('first') + " " + $(this).attr('last') + '" >' + $(this).attr('position') + ". " + $(this).attr('first') + " " + $(this).attr('last') + '</option>';

                            });

                            $(this).find('coach').each(function() {
                                if ($(this).attr('position') == 'manager') {
                                    offense_manager = $(this).attr('first') + ' ' + $(this).attr('last');
                                }
                            });
                        }
                        if ($(this).attr('type') == 'home') {

                            defense_id = $(this).attr('id') + ' Manager ';
                            $(this).find('player').each(function() {
                                if ($('replay_tag_play_variation').val() != 'pickoff') {
                                    //tag play pitchers
                                    defense_players += '<option value="' + $(this).attr('position') + " " + $(this).attr('first') + " " + $(this).attr('last') + '" >' + $(this).attr('position') + ". " + $(this).attr('first') + " " + $(this).attr('last') + '</option>';
                                } 
                                else {
                                    if ($(this).attr('position') == 'P') {

                                    defense_players += '<option value="' + $(this).attr('position') + " " + $(this).attr('first') + " " + $(this).attr('last') + '" >' + $(this).attr('position') + ". " + $(this).attr('first') + " " + $(this).attr('last') + '</option>';
                                    }
                                }
                            });

                            $(this).find('coach').each(function() {
                                if ($(this).attr('position') == 'manager') {
                                    defense_manager = $(this).attr('first') + ' ' + $(this).attr('last');
                                }
                            });
                        }

                    });

                    if ($('.replay_type_of_challenge').val() == 'Crew Chief Review') {

                        whochallenged = '<option value="Crew Chief Review" >Crew Chief Review</option>';

                    } else {



                        // var count = get_data_count('.date_select');
                        // console.log('Defense'+defense_id);
                        // console.log('offence'+offense_id);
                        // console.log($('.challenging_team_output').text());
                        if ($('.challenging_team_output').text() + ' Manager ' == offense_id) {
                            // window.element_ee.data.games.game[count].away_name_abbrev

                            whochallenged = '<option selected="selected" value="' + offense_id + offense_manager + '" >' + offense_id + offense_manager + '</option>';
                            whochallenged += '<option value="' + defense_id + defense_manager + '" >' + defense_id + defense_manager + '</option>';
                        } else {
                            // window.element_ee.data.games.game[count].home_name_abbrev

                            whochallenged = '<option value="' + offense_id + offense_manager + '" >' + offense_id + offense_manager + '</option>';
                            whochallenged += '<option selected="selected" value="' + defense_id + defense_manager + '" >' + defense_id + defense_manager + '</option>';
                        }

                    }

                    select_html('replay_visiting_team_player_name', offense_players);
                    select_html('replay_home_team_player_name', defense_players);
                    select_html('replay_home_team_player_name2', defense_players);
                    select_html('replay_whochallenged', whochallenged);

                    select_html('replay_visiting_team_select', $('.replay_visiting_team_select').html() + team_option);
                    select_html('replay_home_team_select', $('.replay_home_team_select').html() + team_option);

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $('#replay_third_b_umpire').val('');
                    $('#replay_second_b_umpire').val('');
                    $('#replay_first_b_umpire').val('');
                    $('#replay_hp_umpire').val('');
                }
            });

            if ($('.replay_type_of_challenge :selected').val() == 'Crew Chief Review') {
                //var playtype_id = $(this).closest('form').attr('id');
                $('.whochallenged_manager').hide();
            } else {
                //var playtype_id = $(this).closest('form').attr('id');
                $('.whochallenged_crew').hide();
            };

        }
        // GAME pre-selected in EMAIL FORM
        //16th April Last part


    $('.replay_type_of_challenge').on('change', function() {
        var playtype_id = $(this).closest('form').attr('id');
        if ($(this).val() == 'Crew Chief Review') {
            $('#' + playtype_id).find('.whochallenged_crew').show();
            $('#' + playtype_id).find('.whochallenged_manager').hide();
        } else {
            $('#' + playtype_id).find('.whochallenged_crew').hide();
            $('#' + playtype_id).find('.whochallenged_manager').show();
        }
    });
    // 30th March Part#2 Phase#2

    $('.new_replay .btn-default').on('click', function() {

        //validate the form

        if ($(this).val() == 'Generate Email') {
            //var playtype_id = $(this).closest('form').attr('id');
            var output_mail = '';

               //$('form span:visible')

               var not_filled=false;
               $('form span:visible').find('select').each(function(){
                if(($(this).val() == '') || (typeof $(this).val() === 'undefined')){
                 not_filled = true;
                 return false;
                }

               });


               
               if(not_filled == true){
                    alert('Please fill all the fields');
                return false;
               }
               
            var playtype_id = $(this).closest('form').attr('id');
            var output_mail = '';

            var CHALLENGETYPE = $('#' + playtype_id).find('.replay_type_of_challenge').val();
            var GAME = $('#' + playtype_id).find('.date_select :selected').text()
            var INNING = $('#' + playtype_id).find('.replay_inning').val();
            var OUTSBEFORE = $('#' + playtype_id).find('.replay_outs').val();
            var RUNNERSBEFORE = $('#' + playtype_id).find('.replay_runners_before').val();
            var OFFENSETEAM = $('#' + playtype_id).find('.replay_visiting_team').val();
            var OFFENSETEAMSELECT = $('#' + playtype_id).find('.replay_visiting_team_select').val();
            var OFFENSETEAMSELECTED = $('#' + playtype_id).find('.replay_visiting_team_select :selected').val();
            var OFFENSIVEPLAYER = $('#' + playtype_id).find('.replay_visiting_team_player_name').val();
            var OFFENSIVEPLAYERSELECT = $('#' + playtype_id).find('.replay_visiting_team_player_name').val();
            var OFFENSEDOESWHAT = $('#' + playtype_id).find('.replay_offensedoeswhat').val();
            var OFFENSIVEPLAYER_2 = $('#' + playtype_id).find('.replay_visiting_team_player_name_2').val();
            var PLAYLOCATION = $('#' + playtype_id).find('.replay_playlocation').val();
            var PLAYLOCATION_2 = $('#' + playtype_id).find('.replay_playlocation_2').val();
            var PLAYLOCATION_3 = $('#' + playtype_id).find('.replay_playlocation_3').val();
            var CALLONFIELD = $('#' + playtype_id).find('.replay_callonfield').val();
            var CALLONFIELD_2 = $('#' + playtype_id).find('.replay_callonfield_2').val();
            var REPLAY_OFFICIAL = $('#' + playtype_id).find('.replay_replay_official').val();
            var CALLAFTER = $('#' + playtype_id).find('.replay_callonfield_after').val();
            var REPLAY_OFFICIAL_RULING = $('#' + playtype_id).find('.replay_replay_official_ruling  ').val();
            var REPLAY_OFFICIAL_RULING2 = $('#' + playtype_id).find('.replay_replay_official_ruling2    ').val();
            var OUTSAFTER = $('#' + playtype_id).find('.replay_outsafter').val();
            var RUNNERSSCORE = $('#' + playtype_id).find('.replay_runnersscore').val();
            var RUNNERSAFTER = $('#' + playtype_id).find('.replay_runners_on').val();

            var DEFENSIVEPLAYER = $('#' + playtype_id).find('.replay_home_team_player_name').val();
            var DEFENSETEAM = $('#' + playtype_id).find('.replay_home_team').val();
            var DEFENSETEAMSELECT = $('#' + playtype_id).find('.replay_home_team_select ').val();
            var DEFENSIVEPLAYER_2 = $('#' + playtype_id).find('.replay_home_team_player_name2').val();

            var PLAYTYPE = $('#replay_play_type').val();
            var WHOCHALLENGED = $('#' + playtype_id).find(' .replay_whochallenged ').val();

            var RUNNERSSCORE = $('#' + playtype_id).find(' .replay_runnersscore ').val();
            var RUNNERSSCORESELECTED = $('#' + playtype_id).find(' .replay_runnersscore :selected ').val();
            var replay_batter_runner = $('#' + playtype_id).find(' .replay_batter_runner ').val();
            var RUNNERSTARTED = $('#' + playtype_id).find(' .replay_runnerstarted ').val();
            var SAFEOUT = $('#' + playtype_id).find(' .replay_safe_out :selected ').val();
            //homerun
            var ABOVEBELOW = $('#' + playtype_id).find(' .replay_above_below ').val();
            var WOULD = $('#' + playtype_id).find(' .replay_would_wouldnot ').val();
            var HOMERUN_VARIATION = $('#replay_homerun_variation option:selected').val();
            var HOBO = $('.replay_homerun_fair_foul :selected').val();
            var PLAYLOCATIONSTADIUM = $('#var_stadium_boundaries .replay_playlocation :selected').val();
            var PLAYLOCATIONFAN = $('#var_homerun_fan_interference .replay_playlocation :selected').val();
            var PLAYLOCATIONFAIR = $('#var_homerun_fair_foul .replay_playlocation :selected').val();
            //force play
            var FORCE_PLAY_VARIATION = $('#replay_force_play_variation option:selected').val();
            var WIDE_HIGH = $('#' + playtype_id).find(' .replay_wide_high ').val();
            //catch no catch
            var CATCH_NO_CATCH_VARIATION = $('#replay_catch_no_catch_variation option:selected').val();
            var OFFENSE_OUTFIELD_TRANS = $('#var_outfield_transfer .replay_visiting_team_select_2 :selected').val();
            var OFFENSEPLAYER_OUTFIELD_TRANS = $('#var_outfield_transfer .replay_visiting_team_player_name_2 :selected').val();
            var DEFENSE_OUTFIELD_TRANS = $('#var_outfield_transfer .replay_home_team_select_2 :selected').val();
            var DEFENSEPLAYER_OUTFIELD_TRANS = $('#var_outfield_transfer .replay_home_team_player_name_2 :selected').val();
                    //outfield ball off wall
                    var OFFENSE_OUTFIELD_TRANS_BALL = $('#var_outfield_ball_off_wall .replay_visiting_team_select_3 :selected').val();
                    var OFFENSEPLAYER_OUTFIELD_TRANS_BALL = $('#var_outfield_ball_off_wall .replay_visiting_team_player_name_3 :selected').val();
                    var DEFENSE_OUTFIELD_TRANS_BALL = $('#var_outfield_ball_off_wall .replay_home_team_select_3 :selected').val();
                    var DEFENSEPLAYER_OUTFIELD_TRANS_BALL = $('#var_outfield_ball_off_wall .replay_home_team_player_name_3 :selected').val();
                    //
                    var OFFENSE_OUTFIELD_TRAP = $('#var_outfield_trap_play .replay_visiting_team_select_4 :selected').val();
                    var OFFENSEPLAYER_OUTFIELD_TRAP = $('#var_outfield_trap_play .replay_visiting_team_player_name_4 :selected').val();
                    var DEFENSE_OUTFIELD_TRAP = $('#var_outfield_trap_play .replay_home_team_select_4 :selected').val();
                    var DEFENSEPLAYER_OUTFIELD_TRAP = $('#var_outfield_trap_play .replay_home_team_player_name_4 :selected').val();
                    

            // hit by pitch
            var STRIKE = $('#' + playtype_id).find(' .strike ').val();
            // fan interfernce
            var FAN_INTERFERENCE_VARIATION = $('#replay_fan_interference_variation option:selected').val();
            var PLAYERFAN = $('#' + playtype_id).find('.replay_player_fan ').val();
            var CATCHNOCATCH = $('#' + playtype_id).find(' .replay_catchnocatch ').val();
            // tag play
            var FIELDLOCATION_2 = $('#var_batter_attempts_extra .replay_playlocation_2 :selected').val();
            var DIDNOTCOME = $('.replay_did_not_come').val();
            var BATTER_CALLAFTER = $('#var_batter_attempts_extra').find('.replay_callonfield_after').val();
            var CALLONFIELD_RUNNERLOSES = $('#var_runner_loses_contact').find('.replay_callonfield').val();
            var ADVANCEFROM = $('#var_runner_advance').find('.replay_advance_from').val() ;   
            var FIELDLOCATION_3 = $('#var_runner_advance .replay_playlocation_3 :selected').val();
            var CALLAFTER_RULESRUNNER = $('#var_runner_loses_contact .replay_callonfield_after :selected').val();


            if (playtype_id == 'form_homerun') {
                subject_mail = 'Replay Review Alert - Crew Chief Review - ' + GAME + ' - ' + INNING + ' - ' + PLAYTYPE + ', called ' + CALLONFIELD + '(' + REPLAY_OFFICIAL_RULING2 + ')';
                
                if (HOMERUN_VARIATION == 'stadium boundaries') {
                    output_mail =  OFFENSETEAMSELECT + ' ' + OFFENSIVEPLAYER + ' ' + OFFENSEDOESWHAT + ' to deep ' + PLAYLOCATIONSTADIUM + ', ball hits off the top of the wall and bounces back into play, called ' + CALLONFIELD + '. The umpires on the field call for a Crew Chief Review for a potential home run. After review, the replay official rules that the ball struck ' + ABOVEBELOW + ' the home run line. Call ' + REPLAY_OFFICIAL_RULING2 + '. ' ;
                }else if (HOMERUN_VARIATION == 'fan - interference') {
                    output_mail =  OFFENSETEAMSELECT + ' ' + OFFENSIVEPLAYER + ' ' + OFFENSEDOESWHAT + ' to deep ' + PLAYLOCATIONFAN + ', a spectator reaches over the wall and attempts to catch the ball, called ' + CALLONFIELD + '. The umpires on the field call for a Crew Chief Review for a potential home run. After review, the replay official rules that the ball struck ' + WOULD + ' have struck above the home run line, ruled ' +CALLAFTER+'. Call ' + REPLAY_OFFICIAL_RULING2 + '. ' ;
                }else if (HOMERUN_VARIATION == 'fair / foul') {
                    output_mail =  OFFENSETEAMSELECT + ' ' + OFFENSIVEPLAYER + ' ' + OFFENSEDOESWHAT + ' down the ' + PLAYLOCATIONFAIR + ' line, called ' + CALLONFIELD + '. The umpires on the field call for a Crew Chief Review for a potential home run. After review, the replay official rules that the ball was ' + HOBO + ', ruled ' +CALLAFTER+'. Call ' + REPLAY_OFFICIAL_RULING2 + '. ' ;
                };
                
                if (RUNNERSSCORE != 'No runs score') {
                    output_mail +=  RUNNERSSCORE + ', ';
                } 
                output_mail +=  OUTSAFTER + ' out, ' + RUNNERSAFTER + '.';
                
                // end form_homerun
            } else if (playtype_id == 'form_force_play') {
                subject_mail = 'Replay Review Alert - ' + CHALLENGETYPE + ' - ' + GAME + ' - ' + INNING + ' - ' + PLAYTYPE + ' at ' + PLAYLOCATION + ', called ' + CALLONFIELD + '(' + REPLAY_OFFICIAL_RULING + ')';

                

                if (FORCE_PLAY_VARIATION == 'timing') {
                    output_mail =  OFFENSETEAMSELECT + ' ' + OFFENSIVEPLAYER + ' ' + OFFENSEDOESWHAT + ' to ';
                    output_mail += DEFENSETEAMSELECT + ' ' + DEFENSIVEPLAYER + ' who throws to ' + PLAYLOCATION + ', ' + replay_batter_runner + ' called' + CALLONFIELD + 'at ' + PLAYLOCATION_2 + '.';
                }else if (FORCE_PLAY_VARIATION == 'fielder maintaining contact w/bag') {
                    output_mail =  OFFENSETEAMSELECT + ' ' + OFFENSIVEPLAYER + ' ' + OFFENSEDOESWHAT + ' to ';
                    output_mail += DEFENSETEAMSELECT + ' ' + DEFENSIVEPLAYER + ' who throws to ' + PLAYLOCATION + ', throw is '+ WIDE_HIGH + ' pulling ' + $('#var_fielder .replay_home_team_player_name_2').val() + ' off the bag, ' + replay_batter_runner + ' called ' + CALLONFIELD + ' at ' + PLAYLOCATION + '.';
                }else if (FORCE_PLAY_VARIATION == 'double play: call at 1B') {
                    output_mail =  OFFENSETEAMSELECT + ' ' + OFFENSIVEPLAYER + ' ' + OFFENSEDOESWHAT + ' to ';
                     output_mail += DEFENSETEAMSELECT + ' ' + DEFENSIVEPLAYER + ' who throws to ' + DEFENSIVEPLAYER_2 + ' for the out at second base, ' + DEFENSIVEPLAYER_2 + ' throws to 1B, batter/runner called ' + CALLONFIELD + ' at 1B. ';
                };

                
                //output_mail += ' ' + replay_batter_runner + ' ' + CALLONFIELD + ' at ' + PLAYLOCATION_2 + '. ';

                if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires on the field call for a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                } else {
                    output_mail += WHOCHALLENGED + ' challenges the call on the field.';
                }

                output_mail += ' After review, the replay official rules the batter/runner ' + CALLAFTER + ' at ' + $('#form_force_play .replay_playlocation_3').val() + '. Call ' + REPLAY_OFFICIAL_RULING + '. ' ;
                /*
                if ($('#' + playtype_id).find('.replay_callonfield_after').val() == $('#' + playtype_id).find('.replay_callonfield').val()) {

                    output_mail += ' out.';

                } else {
                    output_mail += ' out, '+RUNNERSAFTER+'.';
                }
                */

                if (RUNNERSSCORESELECTED != 'No runs score') {
                    output_mail += RUNNERSSCORE + ', ';
                } 
                output_mail +=  OUTSAFTER + ' out, '  + RUNNERSAFTER + '.';
                
                // end form_force_play
            } else if (playtype_id == 'form_tag_play') {
                subject_mail = 'Replay Review Alert - ' + CHALLENGETYPE + ' - ' + GAME + ' - ' + INNING + ' - ' + PLAYTYPE + ' at ' + PLAYLOCATION + ', called ' + CALLONFIELD + '(' + REPLAY_OFFICIAL_RULING + ')';

                //output_mail = 'We have a ' + CHALLENGETYPE + ' in the ' + GAME + ' game: ' + INNING + ', ' + OUTSBEFORE + ' out, ' + RUNNERSBEFORE + '. ';

                if ($('#replay_tag_play_variation').val() == 'pickoff') {
                    output_mail += DEFENSETEAMSELECT + ' ' + DEFENSIVEPLAYER + ' attempts to pick off ' + OFFENSETEAM + ' runner ' + $('#form_tag_play #var_pickoff').find('.replay_visiting_team_player_name').val() + ' at ' + PLAYLOCATION_2 + '. Runner called ' + $('#var_pickoff .replay_callonfield').val() + ' at ' + $('#' + playtype_id).find('.replay_playlocation_2_1').val() + '. ';
                    if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                    } else {
                        output_mail += WHOCHALLENGED + ' challenges the call on the field.';
                    }
                    output_mail += ' After review, the replay official rules the runner ' + CALLAFTER + ' at ' + PLAYLOCATION_2 + '. Call ' + REPLAY_OFFICIAL_RULING + '. ' ;
                } 

                else if ($('#replay_tag_play_variation').val() == 'stolen base') {
                    output_mail += $('#var_steal .replay_visiting_team_select :selected').val() + ' runner, ' + $('#form_tag_play #var_steal').find('.replay_visiting_team_player_name').val() + ', attempts to steal ' + PLAYLOCATION + ', runner called ' + CALLONFIELD + ' at ' + PLAYLOCATION + '. ';
                    if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                    } else {
                        output_mail += WHOCHALLENGED + ' challenges the call on the field.';
                    }
                    output_mail += ' After review, the replay official rules the runner ' + CALLAFTER + ' at ' + PLAYLOCATION_3 + '. The Call ' + REPLAY_OFFICIAL_RULING + '. ' ;
                }

                else if ($('#replay_tag_play_variation').val() == 'runner loses contact with base') {
                    output_mail += $('#var_runner_loses_contact .replay_home_team_select :selected').val() + ' ' + $('#form_tag_play #var_runner_loses_contact').find('.replay_home_team_player_name').val() + ' attempts to steal ' + PLAYLOCATION + ', runner called ' + CALLONFIELD_RUNNERLOSES + ' at ' + PLAYLOCATION + '. ';
                    if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                    } else {
                        output_mail += WHOCHALLENGED + ' challenges the call on the field.';
                    }
                    output_mail += ' After review, the replay official rules the runner ' + DIDNOTCOME + ' off the bag, rules the runner ' + CALLAFTER_RULESRUNNER +  ' at ' + PLAYLOCATION + '. Call ' + REPLAY_OFFICIAL_RULING + '. ' ;
                }
                

                else if ($('#replay_tag_play_variation').val() == 'batter: attempted extra bases') {
                    output_mail += $('#var_batter_attempts_extra .replay_visiting_team_select :selected').val() + ' ' +  $('#form_tag_play #var_batter_attempts_extra').find('.replay_visiting_team_player_name').val() + ' hits a line drive to ' + FIELDLOCATION_2 + ', batter/runner attempts to advance to ' + PLAYLOCATION + ', ' + $('#var_batter_attempts_extra .replay_home_team').val() + ' ' + $('#var_batter_attempts_extra .replay_home_team_player_name').val() + ' throws to ' + $('#var_batter_attempts_extra .replay_home_team_player_name_2 :selected').val() + ', batter/runner called ' + $('#var_batter_attempts_extra .replay_callonfield').val() + ' at ' + PLAYLOCATION + '. ';
                    if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                    } else {
                        output_mail += WHOCHALLENGED + ' challenges the call on the field.';
                    }
                    output_mail += ' After review, the replay official rules the batter/runner ' + BATTER_CALLAFTER + ' at ' + PLAYLOCATION_3 + '. Call ' + REPLAY_OFFICIAL_RULING + '. ' ;
                }

                else if ($('#replay_tag_play_variation').val() == 'runner: advance between bases') {
                    output_mail += $('#var_runner_advance .replay_visiting_team_select :selected').val() +  $('#form_tag_play #var_runner_advance').find('.replay_visiting_team_player_name').val() + ' hits a line drive to ' + $('#var_runner_advance .replay_playlocation_3').val() + ', runner ' + OFFENSIVEPLAYER + 'attempts to advance from ' + ADVANCEFROM + ' to ' + PLAYLOCATION + ', runner called ' + CALLONFIELD + ' at ' + PLAYLOCATION + '. ';
                    if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                    } else {
                        output_mail += WHOCHALLENGED + ' challenges the call on the field.';
                    }
                    output_mail += ' After review, the replay official rules the runner ' + $('#var_runner_advance .replay_callonfield_after').val() + ' at ' + PLAYLOCATION_3 + '. Call ' + REPLAY_OFFICIAL_RULING + '. ' ;
                }
                else if ($('#replay_tag_play_variation').val() == 'tag-up: attempt to advance') {
                    output_mail += $('#var_tag_up .replay_visiting_team_select :selected').val() + ' ' +  $('#form_tag_play #var_tag_up').find('.replay_visiting_team_player_name').val() + ' hits a line drive to ' + $('#var_tag_up .replay_playlocation_2').val() + ', runner ' + $('#var_tag_up .replay_visiting_team_select :selected').val() + ' ' + $('#var_tag_up .replay_visiting_team_player_name_2').val() +  ' tags up and attempts to advance from ' + $('#var_tag_up .replay_playlocation_3').val() + ' to ' + $('#var_tag_up .replay_playlocation').val() + ', runner called ' + $('#var_tag_up .replay_callonfield').val() + ' at ' + $('#var_tag_up .replay_playlocation').val() + '. ';
                    if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                    } else {
                        output_mail += WHOCHALLENGED + ' challenges the call on the field.';
                    }
                    output_mail += ' After review, the replay official rules the runner ' +$('#var_tag_up .replay_callonfield_after').val()+ '. Call ' + REPLAY_OFFICIAL_RULING + '. ' ;
                }
                else if ($('#replay_tag_play_variation').val() == 'errant throw') {
                    output_mail += $('#form_tag_play #var_errant_throw').find('.replay_visiting_team_select :selected').val() + ' ' +  $('#form_tag_play #var_errant_throw').find('.replay_visiting_team_player_name').val() + ' hits a ground ball to ' + ' ' + $('#form_tag_play #var_errant_throw').find('.replay_home_team_select :selected').val() + ' ' + $('#form_tag_play #var_errant_throw').find('.replay_home_team_player_name').val() + ', who throws to ' + PLAYLOCATION + ', throw is ' + WIDE_HIGH + ' pulling ' + $('#form_tag_play #var_errant_throw').find('.replay_home_team_player_name2').val() + ' off the bag, batter/runner called ' + $('#var_errant_throw .replay_callonfield').val() + ' on the tag at ' + PLAYLOCATION + '. ';
                    if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                    } else {
                        output_mail += WHOCHALLENGED + ' challenges the call on the field.';
                    }
                    output_mail += ' After review, the replay official rules the runner ' + $('#var_errant_throw .replay_callonfield_after_2').val()  + ' at ' + PLAYLOCATION_3 + '. Call ' + REPLAY_OFFICIAL_RULING + '. ' ;
                }


                //if (CHALLENGETYPE == 'Crew Chief Review') {
                //    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                //} else {
                //    output_mail += WHOCHALLENGED + ' challenges the ruling on the field.';
                //}
                //output_mail += ' After review, the replay official rules that the runner is ' + CALLAFTER + ' at ' + PLAYLOCATION_3 + '. The Call ' + REPLAY_OFFICIAL_RULING + '. ' + OUTSAFTER + ' out, ';
                if (RUNNERSSCORE != 'No runs score') {
                    output_mail +=  RUNNERSSCORE + ', ';
                }
                output_mail += OUTSAFTER + ' out, '  + RUNNERSAFTER + '.';
                

                // end form_tag_play
            } else if (playtype_id == 'form_catch_nocatch') {
                subject_mail = 'Replay Review Alert - ' + CHALLENGETYPE + ' - ' + GAME + ' - ' + INNING + ' - ' + PLAYTYPE + ', called ' + CALLONFIELD + '(' + REPLAY_OFFICIAL_RULING + ')';

                if (CATCH_NO_CATCH_VARIATION == 'transfer at 2B') {
                    output_mail =  OFFENSETEAMSELECT + ' ' + OFFENSIVEPLAYER + ' ' + OFFENSEDOESWHAT + ' to ' + DEFENSETEAMSELECT + ' ' + DEFENSIVEPLAYER + ' who throws to ' + $('#var_transfer_at_2B .replay_home_team_player_name_2').val() + ', who drops the ball on the transfer, ' + OFFENSETEAMSELECT + ' runner ' + OFFENSIVEPLAYER + ' called '  + CALLONFIELD + ' at ' + PLAYLOCATION + ', ' + replay_batter_runner + ' to ' + PLAYLOCATION_2 + '. ';
                } else if (CATCH_NO_CATCH_VARIATION == 'outfield: transfer') {
                    output_mail =  OFFENSE_OUTFIELD_TRANS + ' ' + OFFENSEPLAYER_OUTFIELD_TRANS + ' ' + OFFENSEDOESWHAT + ' to ' + DEFENSE_OUTFIELD_TRANS + ' ' + DEFENSEPLAYER_OUTFIELD_TRANS + ', who drops the ball on the transfer, called a ' + CALLONFIELD + '. ';
                } else if (CATCH_NO_CATCH_VARIATION == 'outfield: ball off wall') {
                    output_mail =  OFFENSE_OUTFIELD_TRANS_BALL + ' ' + OFFENSEPLAYER_OUTFIELD_TRANS_BALL + ' hits fly ball to deep ' + PLAYLOCATION + ', ' + DEFENSE_OUTFIELD_TRANS_BALL + ' ' + DEFENSEPLAYER_OUTFIELD_TRANS_BALL + ' attempts to make a catch against the outfield wall, called a ' + CALLONFIELD + '. ';
                } else if (CATCH_NO_CATCH_VARIATION == 'outfield: trap play') {
                   output_mail =  OFFENSE_OUTFIELD_TRAP + ' ' + OFFENSEPLAYER_OUTFIELD_TRAP + ' ' + OFFENSEDOESWHAT + ' to ' + PLAYLOCATION + ', ' + DEFENSE_OUTFIELD_TRAP + ' ' + DEFENSEPLAYER_OUTFIELD_TRAP + ' attempts to make a diving catch, called a ' + CALLONFIELD + '. ';
                };
                

                if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                } else {
                    output_mail += WHOCHALLENGED + ' challenges the call on the field.';
                }

                if (CATCH_NO_CATCH_VARIATION == 'transfer at 2B') {
                    output_mail += ' After review, the replay official rules the runner ' + CALLAFTER + ' at ' + PLAYLOCATION + '. Call ' + REPLAY_OFFICIAL_RULING + '. ';
                } else{
                    output_mail += ' After review, the replay official rules that the play results in a ' + CALLAFTER + '. Call ' + REPLAY_OFFICIAL_RULING + '. ';
                    };
                

                if (RUNNERSSCORE != 'No runs score') {
                    output_mail += RUNNERSSCORE + ', ';
                }
               output_mail +=  OUTSAFTER + ' out, ' +  RUNNERSAFTER + '.';
                    
                // end form_catch_nocatch
            } else if (playtype_id == 'form_fair_foul') {
                subject_mail = 'Replay Review Alert - ' + CHALLENGETYPE + ' - ' + GAME + ' - ' + INNING + ' - ' + PLAYTYPE + ', called ' + CALLONFIELD + '(' + REPLAY_OFFICIAL_RULING + ')';

                output_mail =  OFFENSETEAMSELECT + ' ' + OFFENSIVEPLAYER + ' ' + OFFENSEDOESWHAT + ' down the ' + PLAYLOCATION + ' line, ball is called ' + CALLONFIELD + '. ';

                if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field. ';
                } else {
                    output_mail += WHOCHALLENGED + ' challenges the call on the field.';
                }
                output_mail += ' After review, the replay official rules that the ball is ' + CALLAFTER + '. Call ' + REPLAY_OFFICIAL_RULING + '. ' + OUTSAFTER + ' out, ';


                if (RUNNERSSCORE != 'No runs score') {
                    output_mail += RUNNERSSCORE + ', ';
                }
                output_mail += OUTSAFTER + ' out, ' + RUNNERSAFTER + '.';


                // end form_fair_foul
            } else if (playtype_id == 'form_violation_of_collision') {
                subject_mail = 'Replay Review Alert - ' + CHALLENGETYPE + ' - ' + GAME + ' - ' + INNING + ' - ' + PLAYTYPE + ', called ' + CALLONFIELD + '(' + REPLAY_OFFICIAL_RULING + ')';

                output_mail +=  OFFENSETEAMSELECT + ' ' + OFFENSIVEPLAYER + ' ' + OFFENSEDOESWHAT + ' to ' + PLAYLOCATION + ', runner ' + OFFENSIVEPLAYER_2 + ' attempts to score from ' + RUNNERSTARTED + ', ' + $('#form_violation_of_collision .replay_home_team').val() + ' ' + DEFENSIVEPLAYER + ' throws home to ' + $('#form_violation_of_collision .replay_home_team').val() + ' ' + $('#form_violation_of_collision .replay_home_team_player_name_2').val() +  ', runner called ' + SAFEOUT + ' at ' + PLAYLOCATION + ', ' + CALLONFIELD + '. ';

                if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                } else {
                    output_mail += WHOCHALLENGED + ' challenges the call on the field.';
                }

                output_mail += ' After review, the replay official rules that there was ' + CALLAFTER + ', the runner is ' + SAFEOUT + ' at ' + PLAYLOCATION + '. Call ' + REPLAY_OFFICIAL_RULING + '. ' ;

                if (RUNNERSSCORE != 'No runs score') {
                    output_mail += RUNNERSSCORE + ', ';
                }
                output_mail += OUTSAFTER + ' out, ' + RUNNERSAFTER + '.';


                // end form_violation_of_collision
            } else if (playtype_id == 'form_hit_by_pitch') {
                subject_mail = 'Replay Review Alert - ' + CHALLENGETYPE + ' - ' + GAME + ' - ' + INNING + ' - ' + PLAYTYPE + ', called ' + CALLONFIELD + '(' + REPLAY_OFFICIAL_RULING + ')';

                output_mail =  $('#form_hit_by_pitch').find('.replay_home_team_select ').val() + ' ' + DEFENSIVEPLAYER + ' throws a pitch inside to ' + $('#form_hit_by_pitch .replay_visiting_team_select').val() + ' batter ' + OFFENSIVEPLAYER + ', called ' + CALLONFIELD + '. ';

                if (CHALLENGETYPE == 'Crew Chief Review') {
                    output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                } else {
                    output_mail += WHOCHALLENGED + ' challenges the ruling on the field.';
                }
                output_mail += ' After review, the replay official rules that the ball ' + STRIKE + ' the batter, rules ' + CALLAFTER + '. Call ' + REPLAY_OFFICIAL_RULING + '. ' ;

                if (RUNNERSSCORE != 'No runs score') {
                    output_mail +=  RUNNERSSCORE + ', ';
                }
                output_mail += OUTSAFTER + ' out, ' + RUNNERSAFTER + '.';
                // end form hit by pitch

            } else if (playtype_id == 'form_fan_interference') {
                subject_mail = 'Replay Review Alert - ' + CHALLENGETYPE + ' - ' + GAME + ' - ' + INNING + ' - ' + PLAYTYPE + ', called ' + CALLONFIELD + '(' + REPLAY_OFFICIAL_RULING + ')';

                if (FAN_INTERFERENCE_VARIATION == 'foul ball') {
                    output_mail =  OFFENSETEAMSELECT + ' ' + OFFENSIVEPLAYER + ' hits a line drive down the ' + PLAYLOCATION + ' line ' + DEFENSETEAMSELECT + ' ' + DEFENSIVEPLAYER + ' attempts to make a catch reaching into the ' + PLAYLOCATION + ' stands, ' + PLAYERFAN + ' catches the ball, called ' + CATCHNOCATCH + '. ' 

                    if (CHALLENGETYPE == 'Crew Chief Review') {
                        output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                    } else {
                        output_mail += WHOCHALLENGED + ' challenges the call on the field that there was a ' +CALLONFIELD+ '.';
                    }
                    output_mail += ' After review, the replay official rules that there was ' + CALLAFTER + ', batter ruled out. Call ' + REPLAY_OFFICIAL_RULING + '. ';

                } else if (FAN_INTERFERENCE_VARIATION == 'bounding ball') {
                    output_mail =  OFFENSETEAMSELECT + ' ' + OFFENSIVEPLAYER + '' + OFFENSEDOESWHAT +  ' down the ' + PLAYLOCATION + ' line, called ' + CALLONFIELD + ',' + RUNNERSSCORE + ', batter/runner safe at ' + PLAYLOCATION_2 + '. ' 
                    
                    if (CHALLENGETYPE == 'Crew Chief Review') {
                        output_mail += 'The umpires initiate a Crew Chief review for the ' + CALLONFIELD_2 + ' call on the field.';
                    } else {
                        output_mail += WHOCHALLENGED + ' challenges the call on the field that there was a ' +CALLONFIELD+ '.';
                    }
                    output_mail += ' After review, the replay official rules the ball is ' + CALLAFTER + ' batter/runner safe at ' + PLAYLOCATION_2 + '. Call ' + REPLAY_OFFICIAL_RULING + '. ' ;

                }

                if (RUNNERSSCORE != 'No runs score') {
                    output_mail += RUNNERSSCORE + ', ';
                }
                output_mail +=  OUTSAFTER + ' out, ' + RUNNERSAFTER + '.';

            }


            if (typeof CHALLENGETYPE === 'undefined') {
                CHALLENGETYPE = 'Crew Chief Review';
            }


            $('#email_subject').val(subject_mail);

            $('#email_message').val(output_mail);
        }
        return false;
    });




    // non email generator -->>


    $('#trig').on('click', function() {

        //$('.colPush').toggleClass('col-sm-1 col-sm-4');
        $('.new-replay-header').fadeToggle('fast');
        $('.colPush').toggleClass('active', '').fadeToggle('fast');
        $('#trig .fa').toggleClass('fa-caret-right fa-caret-left');

        $('.colMain').toggleClass('active', '')
    });




    $(function() {
        $('.confirm_ruling_submit input[type=submit]').click(function() {
            $(this).attr('disabled');
        })
    });
    // string replace to XXX/XXX in EMail GAME
    $(".generator .date_select option").each(function() {

        $(this).html($(this).html().substring(11, 21).replace('-', '/').replace('mlb', '').toUpperCase());

    })

    // search video/screenshot links
     $('td').linkify(); 
    //please uncomment the above


    // find select add class
    $('#wrap').find('select').addClass('form-control')

    // matchup show
    $(function() {
        $("[name=matchup]").click(function() {
            $('.toHide').hide();
            $("#blk-" + $(this).val()).fadeIn('slow');
        });
    });


    $(function(){
      var hash = window.location.hash;
      hash && $('ul.nav a[href="' + hash + '"]').tab('show');
    });

    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');
    return $('a[data-toggle="tab"]').on('shown', function(e) {
      return location.hash = $(e.target).attr('href').substr(1);
    });
    
}


$(document).ready(ready);