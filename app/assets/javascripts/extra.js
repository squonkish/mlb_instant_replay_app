var ready = function() {

var txt = $('.replay_game_start_time').val();
getFormattedTime = function (fourDigitTime) {
    var hours24 = parseInt(fourDigitTime.substring(0, 2),10);
    var hours = ((hours24 + 11) % 12) + 1;
    var amPm = hours24 > 11 ? 'pm' : 'am';
    var minutes = fourDigitTime.substring(2);

    return hours + ':' + minutes + amPm;
};
var newTxt = txt.replace(/(\d+)/g, function (match) {
    return getFormattedTime(match)
})
$('.replay_game_start_time').val(newTxt)


//force play disable submit


  $('#replay_time_from_call_to_review_4i option').eq(11).nextAll().remove()
  $('#replay_time_from_call_to_next_pitch_4i option').eq(11).nextAll().remove()
  $('#replay_time_on_headset_4i option').eq(11).nextAll().remove()
  $('#replay_time_from_review_to_ruling_4i option').eq(11).nextAll().remove()  




  $(function() {
    $( "#datepicker" ).datepicker({
      buttonImage: '/images/calendar.png',
        buttonImageOnly: true,
        buttonText : '',
        changeMonth: true,
        changeYear: true,
        showOn: 'button'
    });
  });

	if (window.location.hash == "#replay-review") {
    $("#replay-review").addClass("active in");
	}

// string replace to XXX/XXX in NewReplay GAME
	 $("#replay_game_id option").each(function(){
   
   $(this).html($(this).html().substring(11,21).replace('-','/').replace('mlb', '').toUpperCase());
   
   })

   // replace CHN with CHC
   //$('#wrap').html().text($('#wrap').text().replace('CHN', 'CHC')); 
   //$(".date_select option:contains('CHN')").text().replace("CHN", "CHC");
   $(".date_select option:contains('CHN')").text(function () {
    return $(this).text().replace("CHN", "CHI"); 
		});
   

   $(".date_select option:contains('SLN')").text(function () {
    return $(this).text().replace("SLN", "STL"); 
		});
   $(".date_select option:contains('NYN')").text(function () {
    return $(this).text().replace("NYN", "NYM"); 
		});

   $(".date_select option:contains('NYA')").text(function () {
    return $(this).text().replace("NYA", "NYY"); 
		});

   $(".date_select option:contains('TBA')").text(function () {
    return $(this).text().replace("TBA", "TB"); 
		});

   $(".date_select option:contains('CHA')").text(function () {
    return $(this).text().replace("CHA", "CWS"); 
		});
   $(".date_select option:contains('KCA')").text(function () {
    return $(this).text().replace("KCA", "KC"); 
		});
   $(".date_select option:contains('SFN')").text(function () {
    return $(this).text().replace("SFN", "SF"); 
		});
   $(".date_select option:contains('ANA')").text(function () {
    return $(this).text().replace("ANA", "LAA"); 
		});
   $(".date_select option:contains('SDN')").text(function () {
    return $(this).text().replace("SDN", "SD"); 
		});
   $(".date_select option:contains('LAN')").text(function () {
    return $(this).text().replace("LAN", "LAD"); 
		});
}


$(document).ready(ready);



// Define: Linkify plugin
(function($){

  var url1 = /(^|&lt;|\s)(www\..+?\..+?)(\s|&gt;|$)/g,
      url2 = /(^|&lt;|\s)(((https?|ftp):\/\/|mailto:).+?)(\s|&gt;|$)/g,

      linkifyThis = function () {
        var childNodes = this.childNodes,
            i = childNodes.length;
        while(i--)
        {
          var n = childNodes[i];
          if (n.nodeType == 3) {
            var html = $.trim(n.nodeValue);
            if (html)
            {
              html = html.replace(/&/g, '&amp;')
                         .replace(/</g, '&lt;')
                         .replace(/>/g, '&gt;')
                         .replace(url1, '$1<a target="_blank" href="http://$2">$2</a>$3')
                         .replace(url2, '$1<a target="_blank" href="$2">$2</a>$5');
              $(n).after(html).remove();
            }
          }
          else if (n.nodeType == 1  &&  !/^(a|button|textarea)$/i.test(n.tagName)) {
            linkifyThis.call(n);
          }
        }
      };

  $.fn.linkify = function () {
    return this.each(linkifyThis);
  };

})(jQuery);