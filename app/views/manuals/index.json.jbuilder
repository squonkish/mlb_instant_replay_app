json.array!(@manuals) do |manual|
  json.extract! manual, :id
  json.url manual_url(manual, format: :json)
end
