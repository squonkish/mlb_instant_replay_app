class EmailsController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @all_replays = Replay.where("date >= ?", Date.today)
    @this_replay = @get_id_new_replay = Replay.last.id + 1
    @visiting_team = @all_replays.select("DISTINCT ON (visiting_team) *")
    # emails
    @all_emails = Email.all
    @replay_emails = Email.where("replay_id = ?",  @this_replay).order('id desc')

     #SEARCH GRID
    @grid = initialize_grid(Replay, :order => 'replays.id')
    @grid_weekly = initialize_grid(Replay.where("created_at >= ?", Time.zone.now.beginning_of_week), :order => 'replays.id')



    # GAME DATA
    #@month = Time.current.to_date.strftime('%m')
    #@year = Time.current.to_date.strftime('%Y')
    #@day = Time.current.to_date.strftime('%d')
    #@response = GameFeed.find(@year,@month, @day)
    #@result = @response.parsed_response['data']['games']['game']
     

     # PLAYERS DATA
    #@game_id = @result.map {|v|  v['id']}.first

    #@game_id.gsub!("/", "_")
    #@game_id.gsub!("-", "_")
    #@players_feed = PlayersFeed.find(@year,@month, @day, @game_id).parsed_response
    #@away_team_players = @players_feed['game']['team'][0]
    
  end

   def create
    # emails
    @this_replay = Replay.last.id
    @all_emails = Email.all
    @new_replay = Replay.new
    @new_email = Email.new(new_email_params)
    @new_email.user_id = current_user.id
    @sender = current_user.email
    

    
    if @new_email.save
      flash[:notice] = "Successfully sent email."

      #SEND EMAIL
      email_last = Email.last
      email_cc = email_last.cc.present?
      
      require 'mail'

      options = { 
                  :address              => "smtp.gmail.com",
                  :port                 => 587,
                  :user_name            => 'motorizedmeatpodz@gmail.com',
                  :password             => 'friggyPudding',
                  :authentication       => 'plain',
                  :enable_starttls_auto => true  
      }


      Mail.defaults do
        delivery_method :smtp, options
      end

      Mail.deliver do
             to email_last.receiver_address
           from email_last.sender_address
             cc email_last.cc
        subject email_last.subject
           body email_last.message
      end

      #SAVE UPDATED REPLAY INFO
      #@new_replay = Replay.new
      #@new_replay.update(new_replay_params)
      # AND REDIRECT
      redirect_to replay_path(@this_replay, :anchor => "data")
    else
      flash[:error] = "Please enter a valid email address."
      redirect_to :back
    end
  end

  def show
    @plus = "+".to_s
  end

  def new
    @confirmed = Replay.find(params[:id])

    @new_replay = Replay.find(params[:id])
    @all_replays = Replay.where("date >= ?", Date.today)
    @this_replay = Replay.find(params[:id])
    @visiting_team = @all_replays.select("DISTINCT ON (visiting_team) *")
    
    @date_current = Replay.where("date >= ?", Date.today).order('id desc')
    
    
    
    # emails
    @all_emails = Email.all
    @new_email = Email.new
    @replay_emails = Email.where("replay_id = ?",  @this_replay).order('id desc')

    # GAME DATA
    @month = Time.current.to_date.strftime('%m')
    @year = Time.current.to_date.strftime('%Y')
    @day = Time.current.to_date.strftime('%d')
    @response = GameFeed.find(@year,@month, @day)
    @result = @response.parsed_response['data']['games']['game']
     

     # PLAYERS DATA
    if @result.present?
      redirect_to calendars_path
      flash[:notice] = "There are no valid games to log today."
    else
      @game_id = @result.map {|v|  v['id']}
    
    end

     #SEARCH GRID
    @grid = initialize_grid(Replay, :order => 'replays.id')
    @grid_weekly = initialize_grid(Replay.where("created_at >= ?", Time.zone.now.beginning_of_week), :order => 'replays.id')


  end

  def edit
    @edit_replay = Replay.find(params[:id])
  end

  def update
    # UPDATE REPLAY
    @new_replay = Replay.find(params[:id])
    @this_replay = Replay.find(params[:id])
    @new_replay.update(new_replay_params)
    flash[:notice] = "Successfully updated."
    redirect_to replay_path(@this_replay)
    
  end


  private
        def new_email_params
            params.require(:email)
            .permit(:sender_address, :subject, :receiver_address, :message, :cc, :cc2, :cc3, :cc4, :cc5, :cc6, :cc7, :cc8, :replay_id)
        end 

        def new_replay_params
            params.require(:replay)
            .permit( :type_of_challenge,:date, :game_id, :homerun_variation,
              :visiting_team, :home_team, :hp_umpire, :game_start_time, 
              :first_b_umpire, :second_b_umpire, :third_b_umpire, :replay_official, 
              :inning, :score, :in_favor_of, :outs, :runners_on, :challenging_team, 
              :calling_umpire, :play_desc, :play_type, :headset_ump1, :headset_ump2, :orig_ruling_on_field, 
              :replay_official_ruling, :placement_of_runners, :was_ruling_correct, :time_from_call_to_review,
              :time_on_headset, :time_from_call_to_next_pitch,:time_from_review_to_ruling, :time_of_play, :play_location, :stadium, :admin, 
              :replay_tech, :definitive_angle, :cameras_available, :notes, :video_link, :screenshot_link, :tag_play_variation)
        end
end
