 class ConfirmedController < ApplicationController
  def show
    
  end
  def populate_data_based_on_away_team
    # GAME DATA
    month = Time.current.to_date.strftime('%m')
    year = Time.current.to_date.strftime('%Y')
    day = Time.current.to_date.strftime('%d')
    response = GameFeed.find(year,month, day) #/year_#{year}/month_#{month}/day_#{day}/grid.json
    @result = response.parsed_response['data']['games']['game'] # returns all game data for today
    @date = @result.map {|v| [ v['id']] } # date/id of all games
     # PLAYERS/UMPIRES
    @game_id = @date[0] #first game id
    @game_id.gsub!("/", "_")
    @game_id.gsub!("-", "_")
    @response_players = PlayersFeed.find(@year,@month, @day, @game_id)
    @away_team_players = @response_players.parsed_response['game']['team'][0]['player'] # away team players

    #####
    @update_fields = @response_players.where(@game_id, params[:date])
    respond_to do |format|
      format.js
    end
  end

end