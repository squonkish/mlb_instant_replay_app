class ReplaysController < ApplicationController
  before_filter :authenticate_user!

  def index
    @replay_date = Replay.all
    @all_replays = Replay.where("date >= ?", Date.today)
    @visiting_team = @all_replays.select("DISTINCT ON (visiting_team) *")
    
    @all_replays_order = Replay.all
    respond_to do |format|
      format.html
      format.csv { send_data @all_replays_order.to_csv }
      format.xls #{ send_data @all_replays_order.to_csv(col_sep: "\t") }
    end
  end

  def show
    # REPLAYS
    @all_replays = Replay.where("date >= ?", Date.today)
    @visiting_team = @all_replays.select("DISTINCT ON (visiting_team) *")
    #@team = @all_replays.select{|i| i['visiting_team']}
    @this_replay = Replay.find(params[:id])  
    @confirmed = Replay.find(params[:id])
    @new_replay = Replay.find(params[:id])
    @get_replay_email = Email.where(replay_id:  @this_replay).first
    #@date_current = Replay.where("date >= ?", Date.today).order('id desc')


    # GAME DATA

    
    # EMAILS
    #@replay_emails = Email.where("replay_id = ?",  @this_replay)
   # emails
    @all_emails = Email.all
    @new_email = Email.new
    @replay_emails = Email.where("replay_id = ?",  @this_replay).order('id desc')

  end 

  def new 
    @time_zero = "00:00"
    @all_replays = Replay.where("date >= ?", Date.today)
    @visiting_team = @all_replays.select("DISTINCT ON (visiting_team) *")
    # get id of new replay
    #@get_id_new_replay = Replay.last.id + 1 
    #

    # GAME DATA
    month = Time.current.to_date.strftime('%m')
    year = Time.current.to_date.strftime('%Y')
    get_day = Time.now - 7.hours
    day = get_day.to_date.strftime('%d')
    @day = get_day.to_date.strftime('%d')
    

    response =  GameFeed.find(year,month, day) #/year_#{year}/month_#{month}/day_#{day}/grid.json
    @result = response.parsed_response['data']['games']['game'] # returns all game data for today
    if @result.present?
        redirect_to calendars_path
        flash[:notice] = "There are no valid games to log today."
      else
        @date = @result.map {|v|  v['id'] } # date/id of all games
    end
  
  end 




  def create
    
    # SAVE NEW REPLAY
    @new_replay = Replay.new(new_replay_params)
     # @replays.user_id = current_user.id if current_user

    if @new_replay.save
      flash[:notice] = "Successfully created."
      redirect_to @new_replay
    else
      flash[:error] = "Error: Complete required blue section"
      redirect_to :back
    end

  end 

  def edit
     @all_replays = Replay.where("date >= ?", Date.today)
     @this_replay = Replay.find(params[:id])
     @new_replay = Replay.find(params[:id])
     @visiting_team = @all_replays.select("DISTINCT ON (visiting_team) *")
     # GAME DATA
    month = Time.current.to_date.strftime('%m')
    year = Time.current.to_date.strftime('%Y')
    day = Time.current.to_date.strftime('%d')
    response = GameFeed.find(year,month, day) #/year_#{year}/month_#{month}/day_#{day}/grid.json
    @result = response.parsed_response['data']['games']['game']

     # EMAILS
     @replay_emails = Email.where("replay_id = ?",  @this_replay)

  end

  def update
    @new_replay = Replay.find(params[:id])
    @this_replay = Replay.find(params[:id])
    @new_replay.update(new_replay_params)
    flash[:notice] = "Successfully updated."
    redirect_to replay_path(@this_replay, :anchor => "data")

    
  end   

  def destroy
   @replays = Replay.find(params[:id])
   @replays.destroy
   redirect_to @replays
  end

  require 'time'

def format_time(time)
  # normalize time
  time = time.to_s.rjust(4, '0') if time[0] !~ /[12]/
  time = time.to_s.ljust(4, '0') if time[0] =~ /[12]/

  Time.strptime(time, '%H%M').strftime('%l:%M').strip
end

  private
        def new_email_params
            params.require(:email)
            .permit(:sender_address, :subject, :receiver_address, :message, :cc, :cc2, :cc3, :cc4, :cc5, :cc6, :cc7, :cc8, :replay_id)
        end

        def new_replay_params
            params.require(:replay)
            .permit( :type_of_challenge,:date, :game_id, 
              :visiting_team, :home_team, :hp_umpire, :game_start_time, 
              :first_b_umpire, :second_b_umpire, :third_b_umpire, :replay_official, 
              :inning, :score, :in_favor_of, :outs, :runners_on, :challenging_team, 
              :calling_umpire, :play_desc, :play_type, :headset_ump1, :headset_ump2, :orig_ruling_on_field, 
              :replay_official_ruling, :placement_of_runners, :was_ruling_correct, :time_from_call_to_review,
              :time_on_headset, :time_from_call_to_next_pitch,:time_from_review_to_ruling, :time_of_play, :play_location, :stadium, :admin, 
              :replay_tech, :definitive_angle, :cameras_available, :notes, :video_link, :screenshot_link, :tag_play_variation, :homerun_variation,:force_play_variation,:violation_of_collision_variation, :fair_foul_variation,:catch_no_catch_variation,:hit_by_pitch_variation, :fan_interference_variation)
        end

        

end
