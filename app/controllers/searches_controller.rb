class SearchesController < ApplicationController
  before_action :set_search, only: [:show, :edit, :update, :destroy]

  # GET /searches
  # GET /searches.json
  def index
    @replay_date = Replay.all
    #day_offset = Time.now - (24 * 60 * 60) # 24 hours ago
    @all_replays = Replay.where("date >= ?", Date.today)
    @visiting_team = @all_replays.select("DISTINCT ON (visiting_team) *")
    #@all_replays = Replay.where('created_at > ? AND created_at < ?', Date.today, Date.today + 7.hours)
    
   
     #SEARCH GRID
    @grid = initialize_grid(
      Replay, 
      #:include => [ :visiting_team, :home_team],
      :order => 'replays.id',
      :order_direction => 'desc',
      :custom_order => {
        # 'tasks.priority_id' => 'priorities.name',
        #'Replay.visiting_team'   => 'visiting_teams.position',
        #'tasks.project_id'  => 'projects.name'
      }
      )
    @grid_weekly = initialize_grid(
      Replay.where("created_at >= ?", Time.zone.now.beginning_of_week),
       :order => 'replays.id',
       :order_direction => 'desc'
       )

    @all_replays_order = Replay.all
    respond_to do |format|
      format.html
      
      format.csv { send_data @all_replays_order.to_csv }
      format.xls { send_data @all_replays_order.to_csv(col_sep: "\t") }
    end
  end

  # GET /searches/1
  # GET /searches/1.json
  def show
  end

  # GET /searches/new
  def new
    @search = Search.new
  end

  # GET /searches/1/edit
  def edit
  end

  # POST /searches
  # POST /searches.json
  def create
    @search = Search.new(search_params)

    respond_to do |format|
      if @search.save
        format.html { redirect_to @search, notice: 'Search was successfully created.' }
        format.json { render :show, status: :created, location: @search }
      else
        format.html { render :new }
        format.json { render json: @search.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /searches/1
  # PATCH/PUT /searches/1.json
  def update
    respond_to do |format|
      if @search.update(search_params)
        format.html { redirect_to @search, notice: 'Search was successfully updated.' }
        format.json { render :show, status: :ok, location: @search }
      else
        format.html { render :edit }
        format.json { render json: @search.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /searches/1
  # DELETE /searches/1.json
  def destroy
    @search.destroy
    respond_to do |format|
      format.html { redirect_to searches_url, notice: 'Search was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_search
      @search = Search.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def search_params
      params[:search]
    end
end
