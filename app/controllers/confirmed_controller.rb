 class ConfirmedController < ApplicationController
  before_filter :authenticate_user!
  def index
    @replay_date = Replay.all
    @all_replays = Replay.where("date >= ?", Date.today)
    @visiting_team = @all_replays.select("DISTINCT ON (visiting_team) *")
    
    @all_replays_order = Replay.all
    respond_to do |format|
      format.html
      format.csv { send_data @all_replays_order.to_csv }
    end
  end
  def show
    @replay_date = Replay.all
    @all_replays = Replay.where("date >= ?", Date.today)
    @visiting_team = @all_replays.select("DISTINCT ON (visiting_team) *")
    
    @this_replay = Replay.find(params[:id])
    @confirmed = Replay.find(params[:id])
    @replay_emails = Email.where("replay_id = ?",  @this_replay)
     #SEARCH GRID
    @grid = initialize_grid(Replay, :order => 'replays.id')
    @grid_weekly = initialize_grid(Replay.where("created_at >= ?", Time.zone.now.beginning_of_week), :order => 'replays.id')
    
  end
  def update
    # SAVE CONFIRM RULING
    @this_replay = Replay.find(params[:id])
    @confirmed = Replay.find(params[:id])
    if @confirmed.update_attributes(new_confirm_params)
    
      flash[:notice] = "Successfully verified."
      redirect_to replay_path(@this_replay,:anchor => "data")
    else
      flash[:error] = "Error with verification."
      redirect_to replay_path(@this_replay,:anchor => "data")
    end
  end

  private
        def new_confirm_params
          params.require(:replay).permit(:confirmed_ruling, :confirmation_official)
        end
end