class AddFanIToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :fan_interference_variation, :string
  end
end
