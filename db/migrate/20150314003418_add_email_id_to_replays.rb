class AddEmailIdToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :email_id, :integer
  end
end
