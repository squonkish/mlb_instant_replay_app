class AddTypeOfChallengeToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :type_of_challenge, :string
  end
end
