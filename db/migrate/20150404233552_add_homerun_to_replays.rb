class AddHomerunToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :homerun_variation, :string
  end
end
