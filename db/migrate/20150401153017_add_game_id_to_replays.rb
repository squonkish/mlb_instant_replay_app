class AddGameIdToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :game_id, :string
  end
end
