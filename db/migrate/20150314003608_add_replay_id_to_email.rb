class AddReplayIdToEmail < ActiveRecord::Migration
  def change
    add_column :emails, :replay_id, :integer
  end
end
