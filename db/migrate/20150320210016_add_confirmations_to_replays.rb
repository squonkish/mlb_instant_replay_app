class AddConfirmationsToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :confirmed_ruling, :string
    add_column :replays, :confirmation_official, :string
  end
end
