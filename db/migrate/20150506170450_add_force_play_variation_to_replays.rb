class AddForcePlayVariationToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :force_play_variation, :string
    add_column :replays, :catch_no_catch_variation, :string
    add_column :replays, :fair_foul_variation, :string
    add_column :replays, :violation_of_collision_variation, :string
    add_column :replays, :hit_by_pitch_variation, :string
  end
end
