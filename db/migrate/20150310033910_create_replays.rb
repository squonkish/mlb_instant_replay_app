class CreateReplays < ActiveRecord::Migration
  def change
    create_table :replays do |t|
      t.string :review_number
      t.datetime :date
      t.string :visiting_team
      t.string :home_team
      t.datetime :game_start_time
      t.string :hp_umpire
      t.string :first_b_umpire
      t.string :second_b_umpire
      t.string :third_b_umpire
      t.string :replay_official
      t.string :inning
      t.string :score
      t.string :in_favor_of
      t.integer :outs
      t.string :runners_on
      t.string :challenging_team
      t.string :calling_umpire
      t.text :play_desc
      t.string :play_type
      t.string :headset_ump1
      t.string :headset_ump2
      t.string :orig_ruling_on_field
      t.string :replay_official_ruling
      t.string :placement_of_runners
      t.string :was_ruling_correct
      t.datetime :time_from_call_to_review
      t.datetime :time_from_review_to_ruling
      t.datetime :time_on_headset
      t.datetime :time_from_call_to_next_pitch
      t.datetime :time_of_play
      t.string :play_location
      t.string :stadium
      t.string :admin
      t.string :replay_tech
      t.string :definitive_angle
      t.integer :cameras_available
      t.text :notes
      t.string :video_link
      t.string :screenshot_link
      t.timestamp :created_at
      t.timestamp :updated_at
    end
  end
end
