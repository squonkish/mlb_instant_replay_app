class AddDefensivePlayersToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :defensive_player_two, :string
    add_column :replays, :defensive_player_two_does_what, :string
  end
end
