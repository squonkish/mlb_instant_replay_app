class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
    	t.string :user_id
    	t.string :sender_address
    	t.string :subject
    	t.string :receiver_address
    	t.string :cc
    	t.string :cc2
    	t.string :cc3
    	t.string :cc4
    	t.string :cc5
    	t.string :cc6
    	t.string :cc7
    	t.string :cc8
    	t.text :message
    	t.string :attachment_url

    	t.timestamps
    end
  end
end
