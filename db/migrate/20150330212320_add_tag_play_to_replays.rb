class AddTagPlayToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :tag_play_variation, :string
  end
end
