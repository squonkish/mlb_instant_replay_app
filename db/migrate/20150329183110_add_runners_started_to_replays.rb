class AddRunnersStartedToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :runner_started, :string
  end
end
