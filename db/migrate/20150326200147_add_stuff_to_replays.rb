class AddStuffToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :offensive_player, :string
    add_column :replays, :offensive_player_does_what, :string
    add_column :replays, :defensive_player, :string
    add_column :replays, :defensive_player_does_what, :string
    add_column :replays, :runners_score, :integer
  end
end
