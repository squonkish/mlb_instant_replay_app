class AddCallAfterToReplays < ActiveRecord::Migration
  def change
    add_column :replays, :call_after, :string
  end
end
