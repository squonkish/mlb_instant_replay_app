# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150508211034) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "calendars", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "emails", force: :cascade do |t|
    t.string   "user_id"
    t.string   "sender_address"
    t.string   "subject"
    t.string   "receiver_address"
    t.string   "cc"
    t.string   "cc2"
    t.string   "cc3"
    t.string   "cc4"
    t.string   "cc5"
    t.string   "cc6"
    t.string   "cc7"
    t.string   "cc8"
    t.text     "message"
    t.string   "attachment_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "replay_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "manuals", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.text     "content_md"
    t.text     "content_html"
    t.boolean  "draft",        default: false
    t.integer  "user_id"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "replays", force: :cascade do |t|
    t.string   "review_number"
    t.date     "date"
    t.string   "visiting_team"
    t.string   "home_team"
    t.datetime "game_start_time"
    t.string   "hp_umpire"
    t.string   "first_b_umpire"
    t.string   "second_b_umpire"
    t.string   "third_b_umpire"
    t.string   "replay_official"
    t.string   "inning"
    t.string   "score"
    t.string   "in_favor_of"
    t.integer  "outs"
    t.string   "runners_on"
    t.string   "challenging_team"
    t.string   "calling_umpire"
    t.text     "play_desc"
    t.string   "play_type"
    t.string   "headset_ump1"
    t.string   "headset_ump2"
    t.string   "orig_ruling_on_field"
    t.string   "replay_official_ruling"
    t.string   "placement_of_runners"
    t.string   "was_ruling_correct"
    t.string   "play_location"
    t.string   "stadium"
    t.string   "admin"
    t.string   "replay_tech"
    t.string   "definitive_angle"
    t.integer  "cameras_available"
    t.text     "notes"
    t.string   "video_link"
    t.string   "screenshot_link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "email_id"
    t.string   "type_of_challenge"
    t.string   "confirmed_ruling"
    t.string   "confirmation_official"
    t.string   "offensive_player"
    t.string   "offensive_player_does_what"
    t.string   "defensive_player"
    t.string   "defensive_player_does_what"
    t.integer  "runners_score"
    t.string   "runner_started"
    t.string   "defensive_player_two"
    t.string   "defensive_player_two_does_what"
    t.string   "tag_play_variation"
    t.string   "game_id"
    t.string   "homerun_variation"
    t.datetime "time_on_headset"
    t.datetime "time_of_play"
    t.datetime "time_from_review_to_ruling"
    t.datetime "time_from_call_to_next_pitch"
    t.datetime "time_from_call_to_review"
    t.string   "call_after"
    t.string   "force_play_variation"
    t.string   "catch_no_catch_variation"
    t.string   "fair_foul_variation"
    t.string   "violation_of_collision_variation"
    t.string   "hit_by_pitch_variation"
    t.string   "fan_interference_variation"
  end

  create_table "searches", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "username",               default: "",    null: false
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.boolean  "admin",                  default: false, null: false
    t.boolean  "locked",                 default: false, null: false
    t.string   "slug"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

end
